#pragma once

#include <cassert>
#include <cmath>
#include <cstdint>
#include <type_traits>

namespace xray::math {

namespace detail {

struct fp_type_t {};

struct integer_type_t {};

template <typename T>
using sel_type = std::conditional_t<std::is_floating_point<T>::value, fp_type_t,
                                    integer_type_t>;
}

struct consts {
  template <typename T>
  static constexpr T pi = T{3.14159265359f};

  template <typename T>
  static constexpr T two_pi = T{2.0f} * pi<T>;

  template <typename T>
  static constexpr T half_pi = T{0.5f} * pi<T>;

  template <typename T>
  static constexpr T quarter_pi = T{0.25f} * pi<T>;

  template <typename T>
  static constexpr T epsilon = T{1.0e-5f};
};

namespace detail {

template <typename T>
inline bool is_zero(const T val, detail::fp_type_t) noexcept {
  return std::abs(val) < consts::epsilon<T>;
}

template <typename T>
inline bool is_zero(const T val, detail::integer_type_t) noexcept {
  return val == T{};
}

template <typename T>
inline bool cmp_eq(const T l, const T r, detail::fp_type_t) noexcept {
  return is_zero(l - r, detail::fp_type_t{});
}

template <typename T>
inline bool cmp_eq(const T l, const T r, detail::integer_type_t) noexcept {
  return l == r;
}

template <typename T>
inline bool cmp_ne(const T l, const T r, detail::fp_type_t) noexcept {
  return !cmp_eq(l, r);
}

template <typename T>
inline bool cmp_ne(const T l, const T r, detail::integer_type_t) noexcept {
  return l != r;
}

} // namespace detail

template <typename T>
inline bool is_zero(const T val) noexcept {
  return detail::is_zero(val, detail::sel_type<T>{});
}

template <typename T>
inline bool cmp_eq(const T l, const T r) noexcept {
  return detail::cmp_eq(l, r, detail::sel_type<T>{});
}

template <typename T>
inline bool cmp_ne(const T l, const T r) noexcept {
  return !cmp_eq(l, r);
}

template <typename T>
struct scalar2 {
  union {
    struct {
      T x;
      T y;
    };

    struct {
      T s;
      T t;
    };

    struct {
      T u;
      T v;
    };

    T components[2];
  };

  using class_type = scalar2<T>;

  scalar2() noexcept = default;

  constexpr scalar2(const T x, const T y) noexcept;

  class_type& operator+=(const class_type& rhs) noexcept;

  class_type& operator-=(const class_type& rhs) noexcept;

  class_type& operator*=(const T scalar) noexcept;

  class_type& operator/=(const T scalar) noexcept;

  struct stdc;
};

///////////////////////////////////////////////////////////////////////////////
/// scalar3
///////////////////////////////////////////////////////////////////////////////
template <typename T>
struct scalar3 {

  union {
    struct {
      T x;
      T y;
      T z;
    };

    struct {
      T u;
      T v;
      T w;
    };

    struct {
      T s;
      T t;
      T p;
    };

    T components[3];
  };

  using class_type = scalar3<T>;

  scalar3() noexcept = default;

  constexpr scalar3(const T xval, const T yval, const T zval) noexcept;

  constexpr scalar3(const scalar2<T>& sk, const T zval) noexcept;

  class_type& operator+=(const class_type& rhs) noexcept;

  class_type& operator-=(const class_type& rhs) noexcept;

  class_type& operator*=(const T scalar) noexcept;

  class_type& operator/=(const T scalar) noexcept;

  /// \name Standard constants for R3 vectors.
  /// @{

public:
  struct stdc;

  /// @}
};

///////////////////////////////////////////////////////////////////////////////
/// scalar4x4
///////////////////////////////////////////////////////////////////////////////
template <typename T>
struct scalar4x4 {
  union {
    struct {
      T a00, a01, a02, a03;
      T a10, a11, a12, a13;
      T a20, a21, a22, a23;
      T a30, a31, a32, a33;
    };

    T components[4 * 4];
  };

  scalar4x4() noexcept = default;

  constexpr scalar4x4(const T a11, const T a12, const T a13, const T a14,
                      const T a21, const T a22, const T a23, const T a24,
                      const T a31, const T a32, const T a33, const T a34,
                      const T a41, const T a42, const T a43,
                      const T a44) noexcept;

  struct stdc;
};

////////////////////////////////////////////////////////////////////////////////
///    Transform matrices, 4x4
////////////////////////////////////////////////////////////////////////////////

struct r4 {
  template <typename T>
  static inline scalar4x4<T> translate(const scalar3<T>& ts) noexcept;

  template <typename T>
  static inline scalar4x4<T> translate(const T x, const T y,
                                       const T z) noexcept;
};

////////////////////////////////////////////////////////////////////////////////
///
///    Implementations
///
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
///    Implementation scalar2
////////////////////////////////////////////////////////////////////////////////

template <typename T>
constexpr scalar2<T>::scalar2(const T x_, const T y_) noexcept : x{x_}, y{y_} {}

template <typename T>
struct scalar2<T>::stdc {
  static constexpr scalar2<T> zero{T{}, T{}};
  static constexpr scalar2<T> unit_x{T(1), T{}};
  static constexpr scalar2<T> unit_y{T{}, T(1)};
};

template <typename T>
constexpr scalar2<T> scalar2<T>::stdc::zero;

template <typename T>
constexpr scalar2<T> scalar2<T>::stdc::unit_x;

template <typename T>
constexpr scalar2<T> scalar2<T>::stdc::unit_y;

using vec2     = scalar2<float>;
using dvec2    = scalar2<double>;
using i32vec2  = scalar2<int32_t>;
using ui32vec2 = scalar2<uint32_t>;

template <typename T>
scalar2<T>& scalar2<T>::operator+=(const scalar2<T>& rhs) noexcept {
  x += rhs.x;
  y += rhs.y;
  return *this;
}

template <typename T>
scalar2<T>& scalar2<T>::operator-=(const scalar2<T>& rhs) noexcept {
  x -= rhs.x;
  y -= rhs.y;
  return *this;
}

template <typename T>
scalar2<T>& scalar2<T>::operator*=(const T scalar) noexcept {
  x *= scalar;
  y *= scalar;
  return *this;
}

template <typename T>
scalar2<T>& scalar2<T>::operator/=(const T scalar) noexcept {
  x /= scalar;
  y /= scalar;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////
///    Implementation scalar3
////////////////////////////////////////////////////////////////////////////////

template <typename T>
struct scalar3<T>::stdc {
  static constexpr scalar3<T> unit_x{T(1.0), T(0.0), T(0.0)};
  static constexpr scalar3<T> unit_y{T(0.0), T(1.0), T(0.0)};
  static constexpr scalar3<T> unit_z{T(0.0), T(0.0), T(1.0)};
  static constexpr scalar3<T> zero{T(0.0), T(0.0), T(0.0)};
  static constexpr scalar3<T> one{T(1.0), T(1.0), T(1.0)};
};

template <typename T>
constexpr scalar3<T> scalar3<T>::stdc::unit_x;

template <typename T>
constexpr scalar3<T> scalar3<T>::stdc::unit_y;

template <typename T>
constexpr scalar3<T> scalar3<T>::stdc::unit_z;

template <typename T>
constexpr scalar3<T> scalar3<T>::stdc::zero;

template <typename T>
constexpr scalar3<T> scalar3<T>::stdc::one;

using vec3  = scalar3<float>;
using dvec3 = scalar3<double>;

template <typename T>
constexpr scalar3<T>::scalar3(const T xval, const T yval, const T zval) noexcept
    : x{xval}, y{yval}, z{zval} {}

template <typename T>
constexpr scalar3<T>::scalar3(const scalar2<T>& sk, const T zval) noexcept
    : scalar3{sk.x, sk.y, zval} {}

template <typename T>
scalar3<T>& scalar3<T>::operator+=(const scalar3<T>& rhs) noexcept {
  x += rhs.x;
  y += rhs.y;
  z += rhs.z;
  return *this;
}

template <typename T>
scalar3<T>& scalar3<T>::operator-=(const scalar3<T>& rhs) noexcept {
  x -= rhs.x;
  y -= rhs.y;
  z -= rhs.z;
  return *this;
}

template <typename T>
scalar3<T>& scalar3<T>::operator*=(const T scalar) noexcept {
  x *= scalar;
  y *= scalar;
  z *= scalar;
  return *this;
}

template <typename T>
scalar3<T>& scalar3<T>::operator/=(const T scalar) noexcept {
  x /= scalar;
  y /= scalar;
  z /= scalar;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////
///    Implementation scalar4x4
////////////////////////////////////////////////////////////////////////////////

template <typename T>
struct scalar4x4<T>::stdc {
  /// \brief      Null 4x4 matrix.
  static constexpr scalar4x4<T> null{T(0), T(0), T(0), T(0), T(0), T(0),
                                     T(0), T(0), T(0), T(0), T(0), T(0),
                                     T(0), T(0), T(0), T(0)};

  /// \brief      Identity 4x4 matrix.
  static constexpr scalar4x4<T> identity{T(1), T(0), T(0), T(0), T(0), T(1),
                                         T(0), T(0), T(0), T(0), T(1), T(0),
                                         T(0), T(0), T(0), T(1)};
};

template <typename T>
constexpr scalar4x4<T> scalar4x4<T>::stdc::null;

template <typename T>
constexpr scalar4x4<T> scalar4x4<T>::stdc::identity;

using mat4  = scalar4x4<float>;
using dmat4 = scalar4x4<double>;

template <typename T>
constexpr scalar4x4<T>::scalar4x4(const T a00_, const T a01_, const T a02_,
                                  const T a03_, const T a10_, const T a11_,
                                  const T a12_, const T a13_, const T a20_,
                                  const T a21_, const T a22_, const T a23_,
                                  const T a30_, const T a31_, const T a32_,
                                  const T a33_) noexcept
    : a00{a00_}
    , a01{a01_}
    , a02{a02_}
    , a03{a03_}
    , a10{a10_}
    , a11{a11_}
    , a12{a12_}
    , a13{a13_}
    , a20{a20_}
    , a21{a21_}
    , a22{a22_}
    , a23{a23_}
    , a30{a30_}
    , a31{a31_}
    , a32{a32_}
    , a33{a33_} {}

////////////////////////////////////////////////////////////////////////////////
///    Implementation scalar4x4
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
///    scalar2 algebra
////////////////////////////////////////////////////////////////////////////////

template <typename T>
inline constexpr T dot(const scalar2<T>& a, const scalar2<T>& b) noexcept {
  return a.x * b.x + a.y * b.y;
}

template <typename T>
inline constexpr scalar2<T> perp(const scalar2<T>& s) noexcept {
  return {-s.y, s.x};
}

template <typename T>
inline constexpr T square_length(const scalar2<T>& s) noexcept {
  return dot(s, s);
}

template <typename T>
inline constexpr bool is_zero_length(const scalar2<T>& s) noexcept {
  return is_zero(square_length(s));
}

template <typename T>
inline constexpr bool is_unit_length(const scalar2<T>& s) noexcept {
  return cmp_eq(square_length(s), T(1));
}

template <typename T>
inline constexpr bool are_perpendicular(const scalar2<T>& s0,
                                        const scalar2<T>& s1) noexcept {
  return is_zero(dot(s0, s1));
}

template <typename T>
inline constexpr bool are_parallel(const scalar2<T>& s0,
                                   const scalar2<T>& s1) noexcept {
  return are_perpendicular(s0, perp(s1));
}

template <typename T>
inline constexpr scalar2<T> operator-(const scalar2<T>& s) noexcept {
  return {-s.x, -s.y};
}

////////////////////////////////////////////////////////////////////////////////
///    Projection matrices
////////////////////////////////////////////////////////////////////////////////

struct projection {
  template <typename T>
  static scalar4x4<T> ortographic(const T left, const T right, const T top,
                                  const T bottom, const T znear,
                                  const T zfar) noexcept;
};

////////////////////////////////////////////////////////////////////////////////
///    Projection matrices implementation
////////////////////////////////////////////////////////////////////////////////
template <typename T>
scalar4x4<T> projection::ortographic(const T left, const T right, const T top,
                                     const T bottom, const T znear,
                                     const T zfar) noexcept {
  scalar4x4<T> m;

  m.a00 = T(2) / (right - left);
  m.a01 = T{};
  m.a02 = T{};
  m.a03 = (left + right) / (left - right);

  m.a10 = T{};
  m.a11 = T(2) / (top - bottom);
  m.a12 = T{};
  m.a13 = (bottom + top) / (bottom - top);

  m.a20 = T{};
  m.a21 = T{};
  m.a22 = T(2) / (znear - zfar);
  m.a23 = (znear + zfar) / (zfar - znear);

  m.a30 = T{};
  m.a31 = T{};
  m.a32 = T{};
  m.a33 = T(1);

  return m;
}

////////////////////////////////////////////////////////////////////////////////
///    Implementation transform matrices, 4x4
////////////////////////////////////////////////////////////////////////////////
template <typename T>
inline scalar4x4<T> r4::translate(const T x, const T y, const T z) noexcept {
  // clang-format off
        return {
            T(1), T(0), T(0), x,
            T(0), T(1), T(0), y,
            T(0), T(0), T(1), z
        };
  // clang-format on
}

template <typename T>
inline scalar4x4<T> r4::translate(const scalar3<T>& ts) noexcept {
  return translate(ts.x, ts.y, ts.z);
}

} // namespace xray::math
