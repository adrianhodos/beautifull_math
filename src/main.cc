#include <cassert>
#include <cmath>
#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <memory>
#include <random>
#include <type_traits>
#include <utility>
#include <vector>

#include "gl_api.hpp"
#include <SDL2/SDL.h>

#include "fast_delegate.hpp"
#include "nk_config.hpp"
#include "nuklear.h"
#include "xray_math.hpp"

#if defined(_WIN32)
#include <windows.h>
#endif

#define XR_NO_COPYCTOR_ASSIGNMENT(type)                                        \
  type(const type&) = delete;                                                  \
  type& operator=(const type&) = delete

#define XR_PASTE(x, y) x##y
#define XR_CONCAT(x, y) XR_PASTE(x, y)
#define XR_AUTO_VAR(var_name) XR_PASTE(var_name, __COUNTER__)

namespace lowlevel {

template <typename ExitOp>
struct scope_guard {
public:
  scope_guard(ExitOp exOp) : _exitOp{std::move(exOp)} {}

  ~scope_guard() {
    if (_execOp)
      _exitOp();
  }

  scope_guard(scope_guard<ExitOp>&& rval)
      : _exitOp{std::move(rval._exitOp)}, _execOp{rval._execOp} {
    rval._execOp = false;
  }

private:
  ExitOp _exitOp;
  bool   _execOp{true};
  XR_NO_COPYCTOR_ASSIGNMENT(scope_guard);
};

namespace detail {

enum class scope_guard_helper_type {

};

template <typename ScopeGuardFun>
scope_guard<ScopeGuardFun> operator+(scope_guard_helper_type,
                                     ScopeGuardFun scope_guard_fun) {
  return {std::move(scope_guard_fun)};
}

} // namespace detail

template <typename T, size_t N>
inline constexpr size_t array_dim_helper(const T (&)[N]) noexcept {
  return N;
}

template <typename Tp, typename Dp>
inline typename std::unique_ptr<Tp, Dp>::pointer
raw_ptr(const std::unique_ptr<Tp, Dp>& up) noexcept {
  return up.get();
}

} // namespace lowlevel

#define XR_SCOPE_EXIT()                                                        \
  auto XR_AUTO_VAR(scope_exit_guard) =                                         \
      lowlevel::detail::scope_guard_helper_type{} + [&]

#define XR_ARRAY_SIZE(arr) (lowlevel::array_dim_helper(arr))
#define XR_U32_ARRAY_SIZE(arr)                                                 \
  (static_cast<uint32_t>(lowlevel::array_dim_helper(arr)))
#define XR_I32_ARRAY_SIZE(arr)                                                 \
  (static_cast<int32_t>(lowlevel::array_dim_helper(arr)))

namespace lowlevel {

struct randomNumberGenerator_t {
public:
  enum class distributionType_t : int32_t {
    first,
    uniform = first,
    binomial,
    neg_binomial,
    geometric,
    poisson,
    gamma,
    weibull,
    extreme_value,
    last
  };

  static constexpr const char* const DISTRIBUTION_NAMES[] = {
      "Uniform", "Binomial", "Negative binomial", "Geometric",
      "Poisson", "Gamma",    "Weibull",           "Extreme value"};

public:
  randomNumberGenerator_t() noexcept;

  distributionType_t distribution() const noexcept { return _dist.type; }

  void set_distribution(const distributionType_t dist_type) noexcept {
    _dist.type = dist_type;
  }

  int32_t gen_int32() noexcept;

private:
  std::random_device _device{};
  std::mt19937       _generator;

  struct engineDist_t {
    distributionType_t                     type{distributionType_t::uniform};
    std::uniform_int_distribution<int32_t> uniform;
    std::binomial_distribution<int32_t>    bin;
    std::negative_binomial_distribution<int32_t> neg_bin;
    std::geometric_distribution<int32_t>         geom;
    std::poisson_distribution<int32_t>           poisson;
    std::gamma_distribution<float>               gamma;
    std::weibull_distribution<float>             weibull;
    std::extreme_value_distribution<float>       extval;
  } _dist;
};

constexpr const char* const randomNumberGenerator_t::DISTRIBUTION_NAMES[];

randomNumberGenerator_t::randomNumberGenerator_t() noexcept
    : _generator{_device()} {}

int32_t randomNumberGenerator_t::gen_int32() noexcept {
  switch (_dist.type) {
  case distributionType_t::uniform:
    return _dist.uniform(_generator);
    break;

  case distributionType_t::binomial:
    return _dist.bin(_generator);
    break;

  case distributionType_t::neg_binomial:
    return _dist.neg_bin(_generator);
    break;

  case distributionType_t::geometric:
    return _dist.geom(_generator);
    break;

  case distributionType_t::poisson:
    return _dist.poisson(_generator);
    break;

  case distributionType_t::gamma:
    return static_cast<int32_t>(round(_dist.gamma(_generator)));
    break;

  case distributionType_t::weibull:
    return static_cast<int32_t>(round(_dist.weibull(_generator)));
    break;

  case distributionType_t::extreme_value:
    return static_cast<int32_t>(round(_dist.extval(_generator)));
    break;

  default:
    assert(false && "Unhandled distribution!");
    break;
  }

  return {};
}

void out_dbg_msg(const char* file, const int32_t line, const char* fmt, ...) {
  char tmp_buff[2048];
  snprintf(tmp_buff, XR_ARRAY_SIZE(tmp_buff), "\n[%s] : [%d]", file, line);
  fputs(tmp_buff, stderr);

  va_list args_ptr;
  va_start(args_ptr, fmt);
  vsnprintf(tmp_buff, XR_ARRAY_SIZE(tmp_buff), fmt, args_ptr);
  va_end(args_ptr);
  fputs(tmp_buff, stderr);

  fflush(stderr);
}

} // namespace lowlevel

#define OUT_DBG_MSG(msg, ...)                                                  \
  do {                                                                         \
    lowlevel::out_dbg_msg(__FILE__, __LINE__, msg, ##__VA_ARGS__);             \
  } while (0)

namespace lowlevel {} // namespace lowlevel

namespace rendering {

/*!
 * \brief Initializes SDL in the constructor, calls SDL_Quit() in the
 * destructor.
 */
struct scoped_sdllib_initializer {
  scoped_sdllib_initializer()
      : _success{SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_EVENTS) ==
                 0} {}

  explicit operator bool() const noexcept { return _success; }

  ~scoped_sdllib_initializer() {
    if (_success)
      SDL_Quit();
  }

private:
  bool _success;
  XR_NO_COPYCTOR_ASSIGNMENT(scoped_sdllib_initializer);
};

/*!
 * \brief Deleter for a pointer to a SDL_Window.
 */
struct sdl_window_dtor {
  void operator()(SDL_Window* win) const noexcept {
    if (win)
      SDL_DestroyWindow(win);
  }
};

using scoped_sdl_window = std::unique_ptr<SDL_Window, sdl_window_dtor>;

/*!
 * \brief Deleter for a pointer to an SDL_GLContext.
 */
struct gl_context_dtor {
  void operator()(SDL_GLContext ctx) const noexcept {
    if (ctx)
      SDL_GL_DeleteContext(ctx);
  }
};

using scoped_sdl_gl_context =
    std::unique_ptr<std::remove_pointer<SDL_GLContext>::type, gl_context_dtor>;

/*!
 * \brief Deleter for SDL file I/O object.
 */
struct sdl_rwops_dtor {
  void operator()(SDL_RWops* rwop) const noexcept {
    if (rwop) {
      SDL_RWclose(rwop);
    }
  }
};

using scoped_sdl_rwops = std::unique_ptr<SDL_RWops, sdl_rwops_dtor>;

/*!
 * \brief Wraps a handle to a compiled GLSL shader.
 */
struct shaderObject_t {
public:
  shaderObject_t() noexcept = default;
  shaderObject_t(const char* file_name, const GLenum type) noexcept;
  shaderObject_t(const char* src_code, const size_t code_size,
                 const GLenum type) noexcept;

  shaderObject_t(shaderObject_t&& rval) noexcept {
    take_from_rval(std::forward<shaderObject_t&&>(rval));
  }

  shaderObject_t& operator=(shaderObject_t&& rval) noexcept {
    if (_handle) {
      gl::DeleteShader(_handle);
    }

    take_from_rval(std::forward<shaderObject_t&&>(rval));
    return *this;
  }

  explicit operator bool() const noexcept { return _handle != 0 && _compiled; }

  GLuint handle() const noexcept { return _handle; }
  GLenum type() const noexcept { return _type; }

private:
  void create_from_memory(const char*  src_code,
                          const size_t code_size) noexcept;

  void take_from_rval(shaderObject_t&& rval) noexcept {
    _handle        = rval._handle;
    _type          = rval._type;
    _compiled      = rval._compiled;
    rval._handle   = 0;
    rval._type     = gl::INVALID_VALUE;
    rval._compiled = false;
  }

private:
  GLuint _handle{};
  GLenum _type{gl::INVALID_VALUE};
  bool   _compiled{false};

  XR_NO_COPYCTOR_ASSIGNMENT(shaderObject_t);
};

shaderObject_t::shaderObject_t(const char*  file_name,
                               const GLenum type) noexcept
    : _type{type} {
  scoped_sdl_rwops fh{SDL_RWFromFile(file_name, "r")};
  if (!fh) {
    OUT_DBG_MSG("Failed to open shader file %s", file_name);
    return;
  }

  const auto bytes = SDL_RWsize(lowlevel::raw_ptr(fh));
  if (bytes <= 0) {
    OUT_DBG_MSG("Read file error!");
    return;
  }

  std::vector<char> file_contents;
  file_contents.resize(static_cast<size_t>(bytes + 1), 0);
  SDL_RWread(lowlevel::raw_ptr(fh), &file_contents[0], sizeof(file_contents[0]),
             bytes);

  create_from_memory(&file_contents[0], file_contents.size());
}

shaderObject_t::shaderObject_t(const char* src_code, const size_t code_size,
                               const GLenum type) noexcept
    : _type{type} {
  create_from_memory(src_code, code_size);
}

void shaderObject_t::create_from_memory(const char*  src_code,
                                        const size_t code_size) noexcept {
  _handle = gl::CreateShader(_type);

  const GLint csize = static_cast<GLint>(code_size);
  gl::ShaderSource(_handle, 1, &src_code, &csize);
  gl::CompileShader(_handle);

  _compiled = [sh = _handle]() {
    GLint compile_status{gl::FALSE_};
    gl::GetShaderiv(sh, gl::COMPILE_STATUS, &compile_status);

    return compile_status == gl::TRUE_;
  }
  ();

  if (_compiled)
    return;

  char  tmp_buff[2048];
  GLint log_len{};
  gl::GetShaderInfoLog(_handle, XR_U32_ARRAY_SIZE(tmp_buff), &log_len,
                       tmp_buff);
  tmp_buff[log_len] = 0;

  OUT_DBG_MSG("Shader compile error [[ %s ]]", tmp_buff);
}

enum shaderSrcType_t { file, raw_string };

struct shaderFileSrc_t {
  const char* file_name;
};

struct shaderRawStringSrc_t {
  const char* str;
  size_t      len;

  explicit shaderRawStringSrc_t(const char* src) noexcept
      : str{src}, len{strlen(src)} {}
};

/*!
 * \brief Wraps a handle to an OpenGL pipeline program.
 */
struct drawingProgramComponent_t {
  shaderSrcType_t src_type;
  GLenum          shader_type;
  union {
    shaderFileSrc_t      file;
    shaderRawStringSrc_t str;
  };

  drawingProgramComponent_t() : shader_type{gl::INVALID_VALUE} {}

  drawingProgramComponent_t(const shaderFileSrc_t& file_src,
                            const GLenum           type) noexcept
      : src_type{shaderSrcType_t::file}, shader_type{type}, file{file_src} {}

  drawingProgramComponent_t(const shaderRawStringSrc_t& src,
                            const GLenum                type) noexcept
      : src_type{shaderSrcType_t::raw_string}, shader_type{type}, str{src} {}
};

struct gpuProgramBuilder_t;

struct gpuProgram_t {
public:
  gpuProgram_t() noexcept = default;
  gpuProgram_t(const shaderObject_t* shaders, const size_t count) noexcept;
  gpuProgram_t(const drawingProgramComponent_t* components,
               const size_t                     count) noexcept;

  gpuProgram_t(gpuProgram_t&& rval) noexcept
      : _handle{rval._handle}, _linked{rval._linked} {
    rval._handle = 0;
    rval._linked = false;
  }

  ~gpuProgram_t() noexcept;

  gpuProgram_t& operator=(gpuProgram_t&& rval) noexcept {
    if (_handle) {
      gl::DeleteProgram(_handle);
    }

    _handle      = rval._handle;
    _linked      = rval._linked;
    rval._handle = 0;
    rval._linked = false;

    return *this;
  }

  explicit operator bool() const noexcept { return _handle && _linked; }
  GLuint handle() const noexcept { return _handle; }

private:
  friend struct gpuProgramBuilder_t;

  explicit gpuProgram_t(const GLuint existingHandle) noexcept
      : _handle{existingHandle}
      , _linked{gl::IsProgram(existingHandle) == gl::TRUE_} {}

  void create_from_shaders(const shaderObject_t* shaders,
                           const size_t          count) noexcept;

private:
  GLuint _handle{};
  bool   _linked{false};

  XR_NO_COPYCTOR_ASSIGNMENT(gpuProgram_t);
};

gpuProgram_t::~gpuProgram_t() noexcept {
  if (_handle)
    gl::DeleteProgram(_handle);
}

gpuProgram_t::gpuProgram_t(const shaderObject_t* shaders,
                           const size_t          count) noexcept {
  create_from_shaders(shaders, count);
}

gpuProgram_t::gpuProgram_t(const drawingProgramComponent_t* components,
                           const size_t                     count) noexcept {
  static constexpr size_t MAX_SHADER_COMPONENTS = 5;
  shaderObject_t          shader_objs[MAX_SHADER_COMPONENTS];

  uint32_t cnt{};

  while ((cnt < MAX_SHADER_COMPONENTS) && (cnt < count)) {
    const auto& comp = components[cnt];
    if (comp.src_type == shaderSrcType_t::file) {
      shader_objs[cnt] = shaderObject_t{comp.file.file_name, comp.shader_type};
    } else {
      shader_objs[cnt] =
          shaderObject_t{comp.str.str, comp.str.len, comp.shader_type};
    }

    ++cnt;
  }

  create_from_shaders(shader_objs, cnt);
}

void gpuProgram_t::create_from_shaders(const shaderObject_t* shaders,
                                       const size_t          count) noexcept {
  _handle = gl::CreateProgram();
  for (size_t i = 0; i < count; ++i) {
    if (!shaders[i]) {
      OUT_DBG_MSG("Trying to attach invalid shader to program ! (%d)", i);
      return;
    }

    gl::AttachShader(_handle, shaders[i].handle());
  }

  gl::LinkProgram(_handle);
  _linked = [ph = _handle]() {
    GLint link_succeeded{gl::FALSE_};
    gl::GetProgramiv(ph, gl::LINK_STATUS, &link_succeeded);
    return link_succeeded == gl::TRUE_;
  }
  ();

  if (_linked)
    return;

  char  tmp_buff[2048];
  GLint log_len{};
  gl::GetProgramInfoLog(_handle, XR_U32_ARRAY_SIZE(tmp_buff), &log_len,
                        tmp_buff);
  tmp_buff[log_len] = 0;

  OUT_DBG_MSG("Failed to link program, error [[ % ]]", tmp_buff);
}

enum class graphicsPipelineStage_t : int8_t {
  first,
  vertex = first,
  tess_control,
  tess_eval,
  geometry,
  fragment,
  compute,
  last
};

enum class graphicsPipelineStageBit_t : uint32_t {
  vertex       = (1u << (int8_t) graphicsPipelineStage_t::vertex),
  tess_control = (1u << (int8_t) graphicsPipelineStage_t::tess_control),
  tess_eval    = (1u << (int8_t) graphicsPipelineStage_t::tess_eval),
  geometry     = (1u << (int8_t) graphicsPipelineStage_t::geometry),
  fragment     = (1u << (int8_t) graphicsPipelineStage_t::fragment),
  compute      = (1u << (int8_t) graphicsPipelineStage_t::compute)
};

graphicsPipelineStage_t
mapShaderTypeToPipelineStage(const GLenum shaderType) noexcept {
  switch (shaderType) {
  case gl::VERTEX_SHADER:
    return graphicsPipelineStage_t::vertex;
    break;

  case gl::GEOMETRY_SHADER:
    return graphicsPipelineStage_t::geometry;
    break;

  case gl::TESS_CONTROL_SHADER:
    return graphicsPipelineStage_t::tess_control;
    break;

  case gl::TESS_EVALUATION_SHADER:
    return graphicsPipelineStage_t::tess_eval;
    break;

  case gl::FRAGMENT_SHADER:
    return graphicsPipelineStage_t::fragment;
    break;

  case gl::COMPUTE_SHADER:
    return graphicsPipelineStage_t::compute;
    break;

  default:
    assert(false && "Shader type unmatch to a pipeline stage!");
    break;
  }

  return graphicsPipelineStage_t::last;
}

struct gpuProgramBuilder_t {
public:
  gpuProgramBuilder_t();

  gpuProgramBuilder_t& addShaderFile(const GLenum shaderType,
                                     const char*  filePath) noexcept;

  gpuProgramBuilder_t& addShaderSource(const GLenum shaderType,
                                       const char*  shaderSrc) noexcept;

  gpuProgramBuilder_t&
  addCompiledShader(const shaderObject_t& compiledShader) noexcept;

  gpuProgramBuilder_t& setSeparableHint() noexcept {
    _separable = true;
    return *this;
  }

  gpuProgram_t build() noexcept;

private:
  static constexpr uint32_t kPipelineStages = 6;
  drawingProgramComponent_t _components[kPipelineStages];
  const shaderObject_t*     _compiledShaders[kPipelineStages];
  shaderObject_t            _tempShaders[kPipelineStages];
  bool                      _separable{false};
};

gpuProgramBuilder_t::gpuProgramBuilder_t() {
  memset(_compiledShaders, 0, sizeof(_compiledShaders));
}

gpuProgramBuilder_t&
gpuProgramBuilder_t::addShaderFile(const GLenum shaderType,
                                   const char*  filePath) noexcept {
  _components[(int32_t) mapShaderTypeToPipelineStage(shaderType)] =
      drawingProgramComponent_t{shaderFileSrc_t{filePath}, shaderType};

  return *this;
}

gpuProgramBuilder_t&
gpuProgramBuilder_t::addShaderSource(const GLenum shaderType,
                                     const char*  shaderSrc) noexcept {
  _components[(int32_t) mapShaderTypeToPipelineStage(shaderType)] =
      drawingProgramComponent_t{shaderRawStringSrc_t{shaderSrc}, shaderType};

  return *this;
}

gpuProgramBuilder_t& gpuProgramBuilder_t::addCompiledShader(
    const shaderObject_t& compiledShader) noexcept {
  _compiledShaders[(int32_t) mapShaderTypeToPipelineStage(
      compiledShader.type())] = &compiledShader;

  return *this;
}

gpuProgram_t gpuProgramBuilder_t::build() noexcept {
  GLuint gpuProgHandle{gl::CreateProgram()};

  if (_separable) {
    gl::ProgramParameteri(gpuProgHandle, gl::PROGRAM_SEPARABLE, gl::TRUE_);
  }

  for (uint32_t idx = 0; idx < kPipelineStages; ++idx) {
    if (_compiledShaders[idx] != nullptr) {
      gl::AttachShader(gpuProgHandle, _compiledShaders[idx]->handle());
      continue;
    }

    if (_components[idx].shader_type != gl::INVALID_VALUE) {
      if (_components[idx].src_type == shaderSrcType_t::file) {
        _tempShaders[idx] = shaderObject_t{_components[idx].file.file_name,
                                           _components[idx].shader_type};
      } else {
        _tempShaders[idx] =
            shaderObject_t{_components[idx].str.str, _components[idx].str.len,
                           _components[idx].shader_type};
      }

      gl::AttachShader(gpuProgHandle, _tempShaders[idx].handle());
      continue;
    }
  }

  gl::LinkProgram(gpuProgHandle);
  {
    GLint linkStatus{0};
    gl::GetProgramiv(gpuProgHandle, gl::LINK_STATUS, &linkStatus);
    if (linkStatus == gl::TRUE_)
      return gpuProgram_t{gpuProgHandle};
  }

  {
    char  logTmpBuff[1024];
    GLint bsize{};
    gl::GetProgramInfoLog(gpuProgHandle, XR_ARRAY_SIZE(logTmpBuff), &bsize,
                          logTmpBuff);
    logTmpBuff[bsize] = 0;
    OUT_DBG_MSG("GPU program compile/link error :\n[[ %s ]]", logTmpBuff);
  }

  gl::DeleteProgram(gpuProgHandle);
  return gpuProgram_t{};
}

struct graphicsPipeline_t {
public:
  graphicsPipeline_t() noexcept { gl::CreateProgramPipelines(1, &_phandle); }

  ~graphicsPipeline_t() noexcept {
    if (_phandle)
      gl::DeleteProgramPipelines(1, &_phandle);
  }

  GLuint handle() const noexcept { return _phandle; }

private:
  GLuint _phandle{};
  XR_NO_COPYCTOR_ASSIGNMENT(graphicsPipeline_t);
};

struct graphicsPipelineConfig_t {
public:
  explicit graphicsPipelineConfig_t(const graphicsPipeline_t& pp) noexcept
      : _pipeline{pp.handle()} {}

  graphicsPipelineConfig_t& useStages(const GLbitfield    stages,
                                      const gpuProgram_t* prg) noexcept {
    gl::UseProgramStages(_pipeline, stages, prg ? prg->handle() : 0);
    return *this;
  }

  void bind() const noexcept {
    gl::UseProgram(0);
    gl::BindProgramPipeline(_pipeline);
  }

private:
  GLuint _pipeline{};
};

/*!
 * \brief   Map a buffer range with offset in the constructor and unmap it in
 * the destructor.
 */
struct scopedBufferMapping_t {
public:
  explicit scopedBufferMapping_t(const GLuint buff, const GLbitfield access,
                                 const GLintptr offset = 0,
                                 const GLsizei  length = 0)
      : _buff{buff} {
    const auto mapping_size = length == 0 ? [buff]() {
      GLint bsize{};
      gl::GetNamedBufferParameteriv(buff, gl::BUFFER_SIZE, &bsize);

      return static_cast<GLsizei>(bsize);
    } () : length;

    _mappedptr = gl::MapNamedBufferRange(_buff, offset, mapping_size, access);
  }

  ~scopedBufferMapping_t() noexcept {
    if (_mappedptr)
      gl::UnmapNamedBuffer(_buff);
  }

  void* data() const noexcept { return _mappedptr; }

  explicit operator bool() const noexcept { return _mappedptr != nullptr; }

private:
  GLuint _buff{};
  void*  _mappedptr{nullptr};

private:
  XR_NO_COPYCTOR_ASSIGNMENT(scopedBufferMapping_t);
};

/*!
 * \brief   Save some OpenGL state in the constructor and restore it in the
 *          destructor.
 */
struct openglStateSnapshotRestore_t {
  GLint last_blend_src;
  GLint last_blend_dst;
  GLint last_blend_eq_rgb;
  GLint last_blend_eq_alpha;
  GLint last_viewport[4];
  GLint blend_enabled;
  GLint cullface_enabled;
  GLint depth_enabled;
  GLint scissors_enabled;

  openglStateSnapshotRestore_t() {
    gl::GetIntegerv(gl::BLEND_SRC, &last_blend_src);
    gl::GetIntegerv(gl::BLEND_DST, &last_blend_dst);
    gl::GetIntegerv(gl::BLEND_EQUATION_RGB, &last_blend_eq_rgb);
    gl::GetIntegerv(gl::BLEND_EQUATION_ALPHA, &last_blend_eq_alpha);
    gl::GetIntegerv(gl::VIEWPORT, last_viewport);
    blend_enabled    = gl::IsEnabled(gl::BLEND);
    cullface_enabled = gl::IsEnabled(gl::CULL_FACE);
    depth_enabled    = gl::IsEnabled(gl::DEPTH_TEST);
    scissors_enabled = gl::IsEnabled(gl::SCISSOR_TEST);
  }

  ~openglStateSnapshotRestore_t() {
    gl::BlendEquationSeparate(last_blend_eq_rgb, last_blend_eq_alpha);
    gl::BlendFunc(last_blend_src, last_blend_dst);
    blend_enabled ? gl::Enable(gl::BLEND) : gl::Disable(gl::BLEND);
    cullface_enabled ? gl::Enable(gl::CULL_FACE) : gl::Disable(gl::CULL_FACE);
    depth_enabled ? gl::Enable(gl::DEPTH_TEST) : gl::Disable(gl::DEPTH_TEST);
    scissors_enabled ? gl::Enable(gl::SCISSOR_TEST)
                     : gl::Disable(gl::SCISSOR_TEST);
    gl::Viewport(last_viewport[0], last_viewport[1], last_viewport[2],
                 last_viewport[3]);
  }

private:
  XR_NO_COPYCTOR_ASSIGNMENT(openglStateSnapshotRestore_t);
};

} // namespace rendering

namespace app {

/*!
 * \brief Codes for events that the window responds to.
 */
enum class windowEventCode_t {
  /*!< Animation shape changed by user from the UI panel. */
  kAnimationSwitchByUser,

  /*!< Animation shape changed automatically after the pause interval
     expired. */
  kAnimationSwitchAutomatic
};

union colorType_t {
  SDL_Color sdl_clr;
  uint32_t  u32_clr;

  float red() const noexcept { return static_cast<float>(sdl_clr.r) / 255.0f; }

  float green() const noexcept {
    return static_cast<float>(sdl_clr.g) / 255.0f;
  }

  float blue() const noexcept { return static_cast<float>(sdl_clr.b) / 255.0f; }
};

enum class animationType_t { kTypeA, kTypeB, kTypeC };

class animationRenderer_t {
public:
  struct renderContext_t {
    const xray::math::vec2* verticesLines{};
    size_t                  linesVerticesCount{};
    const xray::math::vec2* verticesEndPoints{};
    size_t                  endPointsVerticesCount{};
    xray::math::mat4        viewProjectionMatrix;
    animationType_t         animType;
  };

  struct renderOptions_t {
    float    endLineLength{2.0f};
    nk_color lineColor{nk_rgb(255, 255, 255)};
    bool     drawEndLine{false};
    nk_color endCapsColor{nk_rgb(200, 0, 0)};
    float    endCapsSize{3.0f};
  };

  animationRenderer_t();
  ~animationRenderer_t();

  bool valid() const noexcept { return _valid; }
  void draw(const renderContext_t* rcon);

  renderOptions_t* getRenderOptions() { return &_renderOpts; }

private:
  GLsizei                 _vertexBufferSize{1024 * sizeof(xray::math::vec2)};
  GLuint                  _vertexBufferLines{};
  GLsizei                 _endPointsBufferSize{1024 * sizeof(xray::math::vec2)};
  GLuint                  _vertexBufferEndPoints{};
  GLuint                  _vertexArrayObject{};
  rendering::gpuProgram_t _vertexProgram;
  rendering::gpuProgram_t _geometryProgram;
  rendering::gpuProgram_t _gsprogEndPoints;
  rendering::gpuProgram_t _fragmentProgram;
  rendering::graphicsPipeline_t _pipelineCfg;
  GLint                         _transformVS{-1};
  GLint                         _transformGS{-1};
  GLint                         _lineColorFS{-1};
  GLint                         _uniformLineLength{-1};
  GLint                         _uniformProjectionMtxGSEndCaps{-1};
  GLint                         _uniformRadiusGSEndCaps{-1};
  renderOptions_t               _renderOpts{};

  explicit operator bool() const noexcept { return _valid; }

private:
  XR_NO_COPYCTOR_ASSIGNMENT(animationRenderer_t);
  bool _valid{false};
};

animationRenderer_t::animationRenderer_t() {
  _vertexBufferLines = [this]() {
    GLuint vbuff{};
    gl::CreateBuffers(1, &vbuff);
    gl::NamedBufferData(vbuff, _vertexBufferSize, nullptr, gl::DYNAMIC_DRAW);
    assert(gl::IsBuffer(vbuff) && "Invalid vertex buffer!");

    return vbuff;
  }();

  _vertexBufferEndPoints = [bytes = static_cast<GLsizei>(_vertexBufferSize)]() {
    GLuint vbuff{};
    gl::CreateBuffers(1, &vbuff);
    gl::NamedBufferData(vbuff, bytes, nullptr, gl::DYNAMIC_DRAW);
    assert(gl::IsBuffer(vbuff) && "Invalid vertex buffer!");

    return vbuff;
  }
  ();

  _vertexArrayObject = [vb = _vertexBufferLines]() {
    GLuint vao{};
    gl::CreateVertexArrays(1, &vao);
    gl::BindVertexArray(vao);
    gl::VertexArrayVertexBuffer(vao, 0, vb, 0, sizeof(xray::math::vec2));
    gl::VertexArrayAttribFormat(vao, 0, 2, gl::FLOAT, gl::FALSE_, 0);
    gl::EnableVertexArrayAttrib(vao, 0);
    gl::VertexArrayAttribBinding(vao, 0, 0);
    gl::BindVertexArray(0);

    assert(gl::IsVertexArray(vao) && "Vertex array object is not valid!");
    return vao;
  }
  ();

  constexpr auto kVertexShaderCode =
      "#version 450 core \n"
      "layout (location = 0) in vec2 vs_in; \n"
      "out gl_PerVertex { \n"
      "   vec4 gl_Position; \n"
      "}; \n"
      "uniform mat4 world_view_proj; \n"
      "void main() { \n"
      "    gl_Position = world_view_proj * vec4(vs_in, 0.0, 1.0); \n"
      "} \n";

  _vertexProgram = rendering::gpuProgramBuilder_t{}
                       .addShaderSource(gl::VERTEX_SHADER, kVertexShaderCode)
                       .setSeparableHint()
                       .build();

  constexpr auto kFragmentShaderCode =
      "#version 450 core \n"
      "layout(location = 0) out vec4 frag_color; \n"
      "uniform vec3 LINE_COLOR; \n"
      "void main() { \n"
      "     frag_color = vec4(LINE_COLOR, 1.0); \n"
      "} \n";

  _fragmentProgram =
      rendering::gpuProgramBuilder_t{}
          .addShaderSource(gl::FRAGMENT_SHADER, kFragmentShaderCode)
          .setSeparableHint()
          .build();

  constexpr auto kGeometryShaderCode =
      "#version 450 core \n"
      ""
      "in gl_PerVertex { \n"
      "    vec4 gl_Position; \n"
      "} gl_in[]; \n"
      ""
      "out gl_PerVertex { \n"
      "   vec4 gl_Position; \n"
      "}; \n"
      ""
      "layout (lines) in; \n"
      "layout (line_strip, max_vertices = 10) out; \n"
      ""
      "uniform mat4 WorldViewProjection; \n"
      "uniform float LineLength; \n"
      ""
      "void main() { \n"
      "    for (int i = 0; i < gl_in.length(); ++i) { \n"
      "        gl_Position = WorldViewProjection * gl_in[i].gl_Position; \n"
      "        EmitVertex();\n"
      "    }\n"
      "    EndPrimitive();\n"
      ""
      "    const vec2 p0 = gl_in[0].gl_Position.xy;\n"
      "    const vec2 p1 = gl_in[1].gl_Position.xy;\n"
      "    const vec2 d0 = normalize(p1 - p0);\n"
      "    const vec2 d1 = vec2(-d0.y, d0.x);\n"
      ""
      "    const vec2 newVerts[] = {\n"
      "            p1 - d0 * LineLength + d1 * LineLength,\n"
      "            p1 + d0 * LineLength + d1 * LineLength,\n"
      "            p1 + d0 * LineLength - d1 * LineLength,\n"
      "            p1 - d0 * LineLength - d1 * LineLength\n"
      "    };\n"
      ""
      "    const int linesIndices[] = {\n"
      "        0, 1,\n"
      "        1, 2,\n"
      "        2, 3,\n"
      "        3, 0\n"
      "    };\n"
      ""
      ""
      "gl_Position = WorldViewProjection * vec4(newVerts[0], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "gl_Position = WorldViewProjection * vec4(newVerts[1], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "EndPrimitive();\n"
      ""
      "gl_Position = WorldViewProjection * vec4(newVerts[1], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "gl_Position = WorldViewProjection * vec4(newVerts[2], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "EndPrimitive();\n"
      ""
      "gl_Position = WorldViewProjection * vec4(newVerts[2], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "gl_Position = WorldViewProjection * vec4(newVerts[3], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "EndPrimitive();\n"
      ""
      "gl_Position = WorldViewProjection * vec4(newVerts[3], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "gl_Position = WorldViewProjection * vec4(newVerts[0], 0.0f, 1.0f);\n"
      "EmitVertex();\n"
      "EndPrimitive();\n"
      ""
      "}\n";

  _geometryProgram =
      rendering::gpuProgramBuilder_t{}
          .addShaderSource(gl::GEOMETRY_SHADER, kGeometryShaderCode)
          .setSeparableHint()
          .build();

  const char* const kGeometryShaderCodeEndCaps =
      "#version 450 core \n"
      ""
      "layout(points) in; \n"
      "layout(triangle_strip, max_vertices = 30) out; \n"
      ""
      "uniform mat4 WorldViewProjection; \n"
      "uniform float Radius; \n"
      ""
      "const float kPI = 3.141592653589f; \n"
      "const float kTwoPI = 2.0f * kPI; \n"
      "const float kSliceRads = kTwoPI / 10.0f; \n"
      ""
      "in gl_PerVertex { \n"
      "    vec4 gl_Position; \n"
      "} gl_in[]; \n"
      ""
      "out gl_PerVertex { \n"
      "     vec4 gl_Position; \n"
      "}; \n"
      ""
      "void main() { \n"
      "    for (int i = 0; i < 10; ++i) { \n"
      "        gl_Position = WorldViewProjection * (gl_in[0].gl_Position); \n"
      "        EmitVertex(); \n"
      ""
      "        const vec2 C = gl_in[0].gl_Position.xy; \n"
      ""
      "        vec2 p0 = C + vec2(Radius * cos(i * kSliceRads), \n"
      "                           Radius * sin(i * kSliceRads)); \n"
      ""
      "        gl_Position = WorldViewProjection * vec4(p0, 0.0f, 1.0f); \n"
      "        EmitVertex(); \n"
      ""
      "        vec2 p1 = C + vec2(Radius * cos((i + 1) * kSliceRads), \n"
      "                           Radius * sin((i + 1) * kSliceRads)); \n"
      ""
      "        gl_Position = WorldViewProjection * vec4(p1, 0.0f, 1.0f); \n"
      "        EmitVertex(); \n"
      ""
      "        EndPrimitive(); \n"
      "    } \n"
      "} \n";

  _gsprogEndPoints =
      rendering::gpuProgramBuilder_t{}
          .addShaderSource(gl::GEOMETRY_SHADER, kGeometryShaderCodeEndCaps)
          .setSeparableHint()
          .build();

  if (!_vertexProgram || !_geometryProgram || !_fragmentProgram ||
      !_gsprogEndPoints) {
    return;
  }

  _transformVS =
      gl::GetUniformLocation(_vertexProgram.handle(), "world_view_proj");
  if (_transformVS == -1) {
    OUT_DBG_MSG("Uniform variable world_view_proj does not exist!");
    return;
  }

  _lineColorFS =
      gl::GetUniformLocation(_fragmentProgram.handle(), "LINE_COLOR");
  if (_lineColorFS == -1) {
    OUT_DBG_MSG("Uniform variable LINE_COLOR does not exist !");
    return;
  }

  _transformGS =
      gl::GetUniformLocation(_geometryProgram.handle(), "WorldViewProjection");
  if (_transformGS == -1) {
    OUT_DBG_MSG("Uniform WorldViewProjection does not exist!");
    return;
  }

  _uniformLineLength =
      gl::GetUniformLocation(_geometryProgram.handle(), "LineLength");
  if (_uniformLineLength == -1) {
    OUT_DBG_MSG("Uniform LineLength does not exist !");
    return;
  }

  _uniformProjectionMtxGSEndCaps =
      gl::GetUniformLocation(_gsprogEndPoints.handle(), "WorldViewProjection");
  assert(_uniformProjectionMtxGSEndCaps != -1 && "Uniform not found!");

  _uniformRadiusGSEndCaps =
      gl::GetUniformLocation(_gsprogEndPoints.handle(), "Radius");
  assert(_uniformRadiusGSEndCaps != -1);

  _valid = true;
}

animationRenderer_t::~animationRenderer_t() {
  if (_vertexBufferLines)
    gl::DeleteBuffers(1, &_vertexBufferLines);

  if (_vertexBufferEndPoints)
    gl::DeleteBuffers(1, &_vertexBufferEndPoints);

  if (_vertexArrayObject)
    gl::DeleteVertexArrays(1, &_vertexArrayObject);
}

void animationRenderer_t::draw(const renderContext_t* rcon) {
  assert(_valid == true);

  const float lineColor[] = {
      static_cast<float>(_renderOpts.lineColor.r) / 255.0f,
      static_cast<float>(_renderOpts.lineColor.g) / 255.0f,
      static_cast<float>(_renderOpts.lineColor.b) / 255.0f};

  const auto bufferSize = static_cast<GLsizei>(rcon->linesVerticesCount *
                                               sizeof(*rcon->verticesLines));
  if (bufferSize > _vertexBufferSize) {
    //
    // reallocation of buffer's data store needed
    const auto realloc_size =
        static_cast<GLsizei>(static_cast<double>(bufferSize) * 1.5);

    gl::NamedBufferData(_vertexBufferLines, realloc_size, nullptr,
                        gl::DYNAMIC_DRAW);
    _vertexBufferSize = realloc_size;
  }

  gl::NamedBufferSubData(_vertexBufferLines, 0, bufferSize,
                         rcon->verticesLines);

  gl::VertexArrayVertexBuffer(_vertexArrayObject, 0, _vertexBufferLines, 0,
                              static_cast<GLsizei>(sizeof(xray::math::vec2)));
  gl::BindVertexArray(_vertexArrayObject);

  gl::ProgramUniform3fv(_fragmentProgram.handle(), _lineColorFS, 1, lineColor);

  const bool drawLineEndPointBoxes =
      _renderOpts.drawEndLine && (rcon->animType != animationType_t::kTypeC);

  if (drawLineEndPointBoxes) {
    gl::ProgramUniform1f(_geometryProgram.handle(), _uniformLineLength,
                         _renderOpts.endLineLength);
    gl::ProgramUniformMatrix4fv(_geometryProgram.handle(), _transformGS, 1,
                                gl::TRUE_,
                                rcon->viewProjectionMatrix.components);
    gl::ProgramUniformMatrix4fv(_vertexProgram.handle(), _transformVS, 1,
                                gl::FALSE_,
                                xray::math::mat4::stdc::identity.components);
  } else {
    gl::ProgramUniformMatrix4fv(_vertexProgram.handle(), _transformVS, 1,
                                gl::TRUE_,
                                rcon->viewProjectionMatrix.components);
  }

  rendering::graphicsPipelineConfig_t{_pipelineCfg}
      .useStages(gl::VERTEX_SHADER_BIT, &_vertexProgram)
      .useStages(gl::GEOMETRY_SHADER_BIT,
                 drawLineEndPointBoxes ? &_geometryProgram : nullptr)
      .useStages(gl::FRAGMENT_SHADER_BIT, &_fragmentProgram)
      .bind();

  gl::DrawArrays(gl::LINE_STRIP, 0,
                 static_cast<GLsizei>(rcon->linesVerticesCount));

  if (rcon->animType == animationType_t::kTypeC) {
    const auto dataSize = static_cast<GLsizei>(rcon->endPointsVerticesCount *
                                               sizeof(xray::math::vec2));

    if (dataSize > _endPointsBufferSize) {
      //
      // Grow the data store of the buffer
      const auto dataStoreSize =
          static_cast<GLsizei>(static_cast<double>(dataSize) * 1.5);
      gl::NamedBufferData(_vertexBufferEndPoints, dataStoreSize, nullptr,
                          gl::DYNAMIC_DRAW);

      _endPointsBufferSize = dataStoreSize;
    }

    gl::ProgramUniformMatrix4fv(_vertexProgram.handle(), _transformVS, 1,
                                gl::FALSE_,
                                xray::math::mat4::stdc::identity.components);

    gl::ProgramUniformMatrix4fv(_gsprogEndPoints.handle(),
                                _uniformProjectionMtxGSEndCaps, 1, gl::TRUE_,
                                rcon->viewProjectionMatrix.components);

    gl::ProgramUniform1f(_gsprogEndPoints.handle(), _uniformRadiusGSEndCaps,
                         _renderOpts.endCapsSize);

    float endCapsColor[4];
    nk_color_fv(&endCapsColor[0], _renderOpts.endCapsColor);

    gl::ProgramUniform3fv(_fragmentProgram.handle(), _lineColorFS, 1,
                          endCapsColor);

    gl::NamedBufferSubData(_vertexBufferEndPoints, 0, dataSize,
                           rcon->verticesEndPoints);

    gl::VertexArrayVertexBuffer(_vertexArrayObject, 0, _vertexBufferEndPoints,
                                0,
                                static_cast<GLsizei>(sizeof(xray::math::vec2)));

    gl::BindVertexArray(_vertexArrayObject);

    rendering::graphicsPipelineConfig_t{_pipelineCfg}
        .useStages(gl::VERTEX_SHADER_BIT, &_vertexProgram)
        .useStages(gl::GEOMETRY_SHADER_BIT, &_gsprogEndPoints)
        .useStages(gl::FRAGMENT_SHADER_BIT, &_fragmentProgram)
        .bind();

    gl::DrawArrays(gl::POINTS, 0,
                   static_cast<GLsizei>(rcon->endPointsVerticesCount));
  }

  gl::BindVertexArray(0);
}

enum class animationState_t { not_started, finished, running, paused };

struct animationUpdateContext_t {
  float                          canvas_x;
  float                          canvas_y;
  std::vector<xray::math::vec2>* linesBuffer;
  std::vector<xray::math::vec2>* endPointsBuffer;
};

struct uiContext_t {
  nk_context*                                            ctx;
  lowlevel::fast_delegate<void(const windowEventCode_t)> animationNotifyEvent;
  lowlevel::fast_delegate<animationRenderer_t::renderOptions_t*()>
      getRenderOptions;
};

class animationBase_t {
public:
  virtual void next_shape() = 0;

protected:
  animationBase_t()          = default;
  virtual ~animationBase_t() = default;

  //
  //    For all animation types
  void uiDrawLineColorControls(const uiContext_t& uiCtx);
  //
  //    Only for animation type A and B
  void uiDrawLineEndPointsControls(const uiContext_t& uiCtx);

  void uiDrawGenFunctionsDef(const uiContext_t& uiCtx, const char* genFunCX,
                             const char* genFunCY, const char* genFuncx,
                             const char* genFuncy);
};

void animationBase_t::uiDrawLineColorControls(const uiContext_t& uiCtx) {
  auto ctx        = uiCtx.ctx;
  auto renderOpts = uiCtx.getRenderOptions();

  nk_layout_row_static(ctx, 32.0f, 128.0f, 1);
  if (nk_button_symbol_label(ctx, NK_SYMBOL_TRIANGLE_RIGHT, "Next shape",
                             NK_TEXT_LEFT)) {
    next_shape();
    uiCtx.animationNotifyEvent(windowEventCode_t::kAnimationSwitchByUser);
  }

  nk_layout_row_dynamic(ctx, 32.0f, 2);
  nk_label(ctx, "Line color", NK_TEXT_LEFT);
  renderOpts->lineColor = nk_color_picker(ctx, renderOpts->lineColor, NK_RGB);

  nk_layout_row_static(ctx, 32.0f, 128.0f, 1);
  if (nk_button_label(ctx, "Reset line color")) {
    renderOpts->lineColor = nk_rgb(255, 255, 255);
  }
}

void animationBase_t::uiDrawLineEndPointsControls(const uiContext_t& uiCtx) {
  auto ctx        = uiCtx.ctx;
  auto renderOpts = uiCtx.getRenderOptions();

  nk_layout_row_dynamic(ctx, 32.0f, 1);
  renderOpts->drawEndLine = nk_check_label(ctx, "Draw end boxes for lines",
                                           renderOpts->drawEndLine) == 1;

  nk_layout_row_dynamic(ctx, 32.0f, 2);

  nk_labelf(ctx, NK_TEXT_LEFT, "End box length [%3.3f]",
            renderOpts->endLineLength);

  renderOpts->endLineLength =
      nk_slide_float(ctx, 2.0f, renderOpts->endLineLength, 16.0f, 0.25f);
}

void animationBase_t::uiDrawGenFunctionsDef(const uiContext_t& uiCtx,
                                            const char*        genFunCX,
                                            const char*        genFunCY,
                                            const char*        genFuncx,
                                            const char*        genFuncy) {
  auto ctx = uiCtx.ctx;

  nk_layout_row_dynamic(ctx, 16.0f, 1);
  nk_label_colored(ctx, genFunCX, NK_TEXT_ALIGN_LEFT, nk_rgb(255, 64, 0));
  nk_label_colored(ctx, genFunCY, NK_TEXT_ALIGN_LEFT, nk_rgb(255, 64, 0));
  nk_label_colored(ctx, genFuncx, NK_TEXT_ALIGN_LEFT, nk_rgb(0, 255, 64));
  nk_label_colored(ctx, genFuncy, NK_TEXT_ALIGN_LEFT, nk_rgb(0, 255, 64));
}

class mathArtAnimation_t : public animationBase_t {
public:
  mathArtAnimation_t(lowlevel::randomNumberGenerator_t* randgen) noexcept;

  void update(const animationUpdateContext_t&) noexcept;

  virtual void next_shape() noexcept override {
    _par.angle = 360.0;
    _state     = animationState_t::not_started;
    generate_background();
  }

  void reset() noexcept {
    _par.angle = 0.0;
    _state     = animationState_t::not_started;
  }

  void options_ui(const uiContext_t& uiCtx) noexcept;

  xray::math::vec2 emit_vertex() const noexcept;

  void generate_background() noexcept {
    if (_rd->gen_int32() % 2 == 0) {
      _par.bkcolor.u32_clr = (4 + _rd->gen_int32() % 5) * 1000;
    } else {
      _par.bkcolor.u32_clr = (13 + _rd->gen_int32() % 6) * 1000;
    }

    _par.bkcolor.u32_clr += 990;
  }

  colorType_t background() const noexcept { return _par.bkcolor; }

  double angle_stepping() const noexcept { return _par.angle_step; }
  void set_angle_stepping(const double val) noexcept { _par.angle_step = val; }

  animationState_t state() const noexcept { return _state; }
  void set_state(const animationState_t state) noexcept { _state = state; }

private:
  struct animationParams_t {
    int32_t     coeff1;
    int32_t     coeff2;
    int32_t     coeff3;
    int32_t     randpt1{2};
    int32_t     randpt2{2};
    int32_t     randpt3{2};
    int32_t     randpt4{2};
    double      CANX{};
    double      CANY{};
    double      angle{};
    double      angle_step{0.25};
    double      CX;
    double      CY;
    double      cx;
    double      cy;
    colorType_t bkcolor;
  };

  lowlevel::randomNumberGenerator_t* _rd;
  animationParams_t                  _par;
  animationState_t                   _state{animationState_t::not_started};
  const char*                        _fun_cx{""};
  const char*                        _fun_cy{""};
  const char*                        _fun_CX{""};
  const char*                        _fun_CY{""};
  int32_t                            _stepping_sel{1};
};

mathArtAnimation_t::mathArtAnimation_t(
    lowlevel::randomNumberGenerator_t* randgen) noexcept
    : _rd{randgen} {
  _par.coeff1 = -7 + _rd->gen_int32() % 6;
  _par.coeff2 = -7 + _rd->gen_int32() % 14;
  _par.coeff3 = -10 + _rd->gen_int32() % 16;
  generate_background();
}

void mathArtAnimation_t::update(
    const animationUpdateContext_t& updateCtx) noexcept {
  _par.CANX = updateCtx.canvas_x;
  _par.CANY = updateCtx.canvas_y;

  if (_state != animationState_t::running) {
    return;
  }

  if (_par.angle == 360.0) {
    _par.coeff1  = -7 + _rd->gen_int32() % 6;
    _par.coeff2  = -7 + _rd->gen_int32() % 14;
    _par.coeff3  = -7 + _rd->gen_int32() % 16;
    _par.angle   = 0.0;
    _par.randpt1 = _rd->gen_int32() % 25;
    _par.randpt2 = _rd->gen_int32() % 25;
    _par.randpt3 = _rd->gen_int32() % 25;
    _par.randpt4 = _rd->gen_int32() % 25;
  }

  _par.angle += _par.angle_step;
  _par.CX = _par.CANX * 0.5 + _par.CANY * 0.25 * sin(_par.angle);
  _par.CY = _par.CANY * 0.5 + _par.CANY * 0.25 * cos(_par.angle);

  _fun_CX = "CX = W + 0.5 * H * 0.25 * sin(A)";
  _fun_CY = "CY = H + 0.5 * H * 0.25 * cos(A)";

  if (_par.randpt1 % 2 == 0) {
    _par.CX =
        _par.CANX * 0.5 + _par.CANY * 0.25 * sin(_par.angle * _par.coeff1);
    _fun_CX = "CX = W * 0.5 + H * 0.25 * sin(A * C1)";
  }

  if (_par.randpt1 % 3 == 0) {
    _par.CY =
        _par.CANY * 0.5 + _par.CANY * 0.25 * cos(_par.angle * _par.coeff1);
    _fun_CY = "CY = H * 0.5 + H * 0.25 * cos(A * C1)";
  }

  _par.cx = _par.CX + (_par.CANY / 7.0) * sin(_par.angle * _par.coeff2);
  _par.cy = _par.CY + (_par.CANY / 7.0) * cos(_par.angle * _par.coeff2);

  _fun_cx = "cx = CX + (H / 7) * sin(A * C2)";
  _fun_cy = "cy = CY + (H / 7) * cos(A * C2)";

  if (_par.randpt2 % 2 == 0) {
    _par.cx = _par.CX +
              (_par.CANY / 7.0) * sin(_par.angle * _par.coeff1) *
                  cos(_par.angle * _par.coeff2);
    _fun_cx = "cx = CX + (H / 7) * sin(A * C1) * cos(A * C2)";
  }

  if (_par.randpt2 % 3 == 0) {
    _par.cy = _par.CY +
              (_par.CANY / 7.0) * cos(_par.angle * _par.coeff1) *
                  sin(_par.angle * _par.coeff2);
    _fun_cy = "cy = CY + (H / 7) * cos(A * C1) * sin(A * C2)";
  }

  if (_par.randpt2 % 4 == 0) {
    _par.cx = _par.CX + (_par.CANY / 7.0) * cos(_par.angle * _par.coeff2);
    _fun_cx = "cx = CX + (H / 7) * cos(A * C2)";
  }

  if (_par.randpt2 % 5 == 0) {
    _par.cy = _par.CY + (_par.CANY / 7.0) * sin(_par.angle * _par.coeff2);
    _fun_cy = "cy = CY + (H / 7) * sin(A * C2)";
  }

  if (_par.randpt3 % 4 == 0) {
    _par.cx = _par.CX + (_par.CANY / 15.0) * sin(_par.angle * _par.coeff3);
    _fun_cx = "cx = CX + (H / 15) * sin(A * C3)";
  } else if (_par.randpt3 % 3 == 0) {
    _par.cx = _par.CX + (_par.CANY / 20.0) * cos(_par.angle * _par.coeff3);
    _fun_cx = "cx = CX + (H / 20) * cos(A * C3)";
  } else if (_par.randpt3 % 2 == 0) {
    _par.cx = _par.CX +
              (_par.CANY / 15.0) * sin(_par.angle * _par.coeff2) *
                  cos(_par.angle * _par.coeff3);
    _fun_cx = "cx = CX + (H / 15) * sin(A * C2) * cos(A * C3)";
  }

  if (_par.randpt4 % 4 == 0) {
    _par.cy = _par.CY + (_par.CANY / 15.0) * cos(_par.angle * _par.coeff3);
    _fun_cy = "cy = CY + (H / 15) * cos(A * C3)";
  } else if (_par.randpt4 % 3 == 0) {
    _par.cy = _par.CY + (_par.CANY / 20.0) * sin(_par.angle * _par.coeff3);
    _fun_cy = "cy = CY + (H / 20) * sin(A * C3)";
  } else if (_par.randpt4 % 2 == 0) {
    _par.cy = _par.CY +
              (_par.CANY / 15.0) * cos(_par.angle * _par.coeff2) *
                  sin(_par.angle * _par.coeff3);
    _fun_cy = "cy = CY + (H / 15) * cos(A * C2) * sin(A * C3)";
  }

  updateCtx.linesBuffer->push_back(emit_vertex());

  if (_par.angle == 360.0) {
    _state = animationState_t::finished;
  }
}

xray::math::vec2 mathArtAnimation_t::emit_vertex() const noexcept {
  return {static_cast<float>(_par.cx), static_cast<float>(_par.cy)};
}

void mathArtAnimation_t::options_ui(const uiContext_t& uiCtx) noexcept {
  auto ctx = uiCtx.ctx;
  nk_layout_row_static(ctx, 348.0f, 384.0f, 1);

  nk_panel animTab;
  if (nk_group_begin(ctx, &animTab, "Animation options",
                     NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_BORDER |
                         NK_WINDOW_TITLE)) {

    uiDrawLineColorControls(uiCtx);
    uiDrawLineEndPointsControls(uiCtx);

    const float allowedSteppings[] = {0.25f, 0.50f, 0.75f};
    const auto  new_stepping =
        nk_propertyi(ctx, "Angle step", 0, _stepping_sel, 2, 1, 1.0f);

    if (new_stepping != _stepping_sel) {
      _stepping_sel   = new_stepping;
      _par.angle_step = allowedSteppings[_stepping_sel];
      reset();
    }

    uiDrawGenFunctionsDef(uiCtx, _fun_CX, _fun_CY, _fun_cx, _fun_cy);
    nk_group_end(ctx);
  }
}

class mathArtAnimation2_t : public animationBase_t {
public:
  mathArtAnimation2_t(lowlevel::randomNumberGenerator_t* randgen) noexcept;

  void update(const animationUpdateContext_t& updateCtx);

  void        generate_background() noexcept;
  colorType_t background() const noexcept { return bk_color; }

  xray::math::vec2 emit_vertex() const noexcept {
    return {static_cast<float>(cx), static_cast<float>(cy)};
  }

  virtual void next_shape() noexcept override {
    ang    = 720.0;
    state_ = animationState_t::not_started;
    generate_background();
  }

  void reset() noexcept {
    ang    = 0.0;
    state_ = animationState_t::not_started;
  }

  void options_ui(const uiContext_t& uiCtx) noexcept;

  animationState_t state() const noexcept { return state_; }
  void set_state(const animationState_t state) noexcept { state_ = state; }

private:
  int32_t                            coeff{};
  int32_t                            coeff2{};
  int32_t                            rand{};
  int32_t                            rand2{};
  double                             ang{};
  double                             CX;
  double                             CY;
  double                             cx;
  double                             cy;
  double                             W;
  double                             H;
  colorType_t                        bk_color;
  animationState_t                   state_{animationState_t::not_started};
  lowlevel::randomNumberGenerator_t* RNG;
  const char*                        fn_CX{""};
  const char*                        fn_CY{""};
  const char*                        fn_cx{""};
  const char*                        fn_cy{""};
};

mathArtAnimation2_t::mathArtAnimation2_t(
    lowlevel::randomNumberGenerator_t* randgen) noexcept
    : RNG{randgen} {
  coeff  = -13 + RNG->gen_int32() % 10;
  rand   = RNG->gen_int32() % 12;
  coeff2 = -13 + RNG->gen_int32() % 10;
  rand2  = RNG->gen_int32() % 12;
}

void mathArtAnimation2_t::update(const animationUpdateContext_t& updateCtx) {
  this->W = updateCtx.canvas_x;
  this->H = updateCtx.canvas_y;

  if (state() != animationState_t::running)
    return;

  if (xray::math::cmp_eq(ang, 720.0)) {
    coeff  = -17 + RNG->gen_int32() % 15;
    rand   = RNG->gen_int32() % 12;
    coeff2 = -17 + RNG->gen_int32() % 15;
    rand2  = RNG->gen_int32() % 12;
    ang    = 0.0;
  }

  ang += 0.5;

  if (rand % 6 == 0) {
    CX    = (W * 0.5) + (H * 0.25) * sin(ang * coeff);
    CY    = (H * 0.5) + (H * 0.25) * cos(ang * coeff);
    fn_CX = "CX = (W * 0.5) + (H * 0.25) * sin(A * C)";
    fn_CY = "CY = (H * 0.5) + (H * 0.25) * cos(A * C)";
  } else if (rand % 5 == 0) {
    CX    = (W * 0.5) + (H * 0.25) * sin(ang * coeff) * cos(ang * coeff);
    CY    = (H * 0.5) + (H * 0.25) * cos(ang * coeff);
    fn_CX = "CX = (W * 0.5) + (H * 0.25) * sin(A * C) * cos(A * C)";
    fn_CY = "CY = (H * 0.5) + (H * 0.25) * cos(A * C)";
  } else if (rand % 4 == 0) {
    CX    = (W * 0.5) + (H * 0.25) * cos(ang * coeff);
    CY    = (H * 0.5) + (H * 0.25) * sin(ang * coeff);
    fn_CX = "CX = (W * 0.5) + (H * 0.25) * cos(ang * coeff)";
    fn_CY = "CY = (H * 0.5)+(H * 0.25) * sin(ang * coeff)";
  } else if (rand % 3 == 0) {
    CX    = (W * 0.5) + (W * 0.25) * sin(ang * coeff);
    CY    = (H * 0.5) + (H * 0.25) * cos(ang * coeff) * sin(ang * coeff);
    fn_CX = "CX = (W * 0.5) + (W * 0.25) * sin(A * C)";
    fn_CY = "CY = (H * 0.5) + (H * 0.25) * cos(A * C) * sin(A * C)";
  } else if (rand % 2 == 0) {
    CX    = (W * 0.5) + (H * 0.25) * cos(ang * coeff);
    CY    = (H * 0.5) + (H * 0.25) * sin(ang * coeff);
    fn_CX = "CX = (W * 0.5) + (H * 0.25) * cos(A * C)";
    fn_CY = "CY = (H * 0.5) + (H * 0.25) * sin(A * C)";
  } else {
    CX    = (W * 0.5) + (W * 0.25) * sin(ang * coeff);
    CY    = (H * 0.5) + (H * 0.25) * cos(ang * coeff);
    fn_CX = "CX = (W * 0.5) + (W * 0.25) * sin(A * C)";
    fn_CY = "CY = (H * 0.5) + (H * 0.25) * cos(A * C)";
  }

  if (rand2 % 6 == 0) {
    cx    = CX + (H / 5.0) * sin(ang * coeff2);
    cy    = CY + (H / 5.0) * cos(ang * coeff2);
    fn_cx = "cx = CX + (H / 5.0) * sin(A * C2)";
    fn_cy = "cy = CY + (H / 5.0) * cos(A * C2)";
  } else if (rand2 % 5 == 0) {
    cx    = CX + (W / 5.0) * sin(ang * coeff2) * cos(ang * coeff2);
    cy    = CY + (H / 5.0) * cos(ang * coeff2);
    fn_cx = "cx = CX + (W / 5.0) * sin(A * C2) * cos(A * C2)";
    fn_cy = "cy = CY + (H / 5.0) * cos(A * C2)";
  } else if (rand2 % 4 == 0) {
    cx    = CX + (W / 5.0) * cos(ang * coeff2);
    cy    = CY + (H / 5.0) * sin(ang * coeff2);
    fn_cx = "cx = CX + (W / 5.0) * cos(A * C2)";
    fn_cy = "cy = CY + (H / 5.0) * sin(A * C2)";
  } else if (rand2 % 3 == 0) {
    cx    = CX + (W / 5.0) * sin(ang * coeff2);
    cy    = CY + (H / 5.0) * cos(ang * coeff2) * sin(ang * coeff2);
    fn_cx = "cx = CX + (W / 5.0) * sin(A * C2)";
    fn_cy = "cy = CY + (H / 5.0) * cos(A * C2) * sin(A * C2)";
  } else if (rand2 % 2 == 0) {
    cx    = CX + (H / 5.0) * cos(ang * coeff2);
    cy    = CY + (H / 5.0) * sin(ang * coeff2);
    fn_cx = "cx = CX + (H / 5.0) * cos(A * C2)";
    fn_cy = "cy = CY + (H / 5.0) * sin(A * C2)";
  } else {
    cx    = CX + (W / 5.0) * sin(ang * coeff2);
    cy    = CY + (H / 5.0) * cos(ang * coeff2);
    fn_cx = "cx = CX + (W / 5.0) * sin(A * C2)";
    fn_cy = "cy = CY + (H / 5.0) * cos(A * C2)";
  }

  updateCtx.linesBuffer->push_back(emit_vertex());

  if (xray::math::cmp_eq(ang, 720.0)) {
    set_state(animationState_t::finished);
  }
}

void mathArtAnimation2_t::generate_background() noexcept {
  if (RNG->gen_int32() % 2 == 0) {
    bk_color.u32_clr = (4 + RNG->gen_int32() % 5) * 1000;
  } else {
    bk_color.u32_clr = (13 + RNG->gen_int32() % 6) * 1000;
  }

  bk_color.u32_clr += 990;
}

void mathArtAnimation2_t::options_ui(const uiContext_t& uiCtx) noexcept {
  auto     ctx = uiCtx.ctx;
  nk_panel animTab;
  nk_layout_row_static(ctx, 348.0f, 384.0f, 1);

  if (nk_group_begin(ctx, &animTab, "Animation options",
                     NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_BORDER |
                         NK_WINDOW_TITLE)) {

    uiDrawLineColorControls(uiCtx);
    uiDrawLineEndPointsControls(uiCtx);
    uiDrawGenFunctionsDef(uiCtx, fn_CX, fn_CY, fn_cx, fn_cy);
    nk_group_end(ctx);
  }
}

class mathArtAnimation3_t : public animationBase_t {
public:
  mathArtAnimation3_t(lowlevel::randomNumberGenerator_t* randgen) noexcept;

  void update(const animationUpdateContext_t& updateCtx);

  void        generate_background() noexcept;
  colorType_t background() const noexcept { return _backgroundColor; }

  virtual void next_shape() noexcept override {
    _a         = 20.0;
    _animState = animationState_t::not_started;
    generate_background();
  }

  void reset() noexcept {
    _a         = 20.0;
    _animState = animationState_t::not_started;
  }

  void options_ui(const uiContext_t& uiCtx) noexcept;

  animationState_t state() const noexcept { return _animState; }
  void set_state(const animationState_t state) noexcept { _animState = state; }

private:
  void emit_vertex(const double x, const double y,
                   std::vector<xray::math::vec2>* buffer);

  void emit_endpoint(const double x0, const double y0,
                     std::vector<xray::math::vec2>* buffer);

  lowlevel::randomNumberGenerator_t* _rand;
  double                             _a{18.9};
  double                             _x;
  double                             _y;
  double                             _W;
  double                             _H;
  int32_t                            _k;
  int32_t                            _l;
  int32_t                            _m;
  int32_t                            _n;
  int32_t                            _rnd{1};
  int32_t                            _Rnd{1};
  int32_t                            _RND{1};
  int32_t                            _k1;
  animationState_t                   _animState{animationState_t::not_started};
  colorType_t                        _backgroundColor;
};

mathArtAnimation3_t::mathArtAnimation3_t(
    lowlevel::randomNumberGenerator_t* randgen) noexcept
    : _rand{randgen} {
  _k  = -8 + _rand->gen_int32() % 17;
  _l  = -16 + _rand->gen_int32() % 32;
  _m  = -16 + _rand->gen_int32() % 40;
  _k1 = _k;
}

void mathArtAnimation3_t::update(const animationUpdateContext_t& updateCtx) {
  _W = static_cast<double>(updateCtx.canvas_x);
  _H = static_cast<double>(updateCtx.canvas_y);

  if (_animState != animationState_t::running)
    return;

  //
  // reset
  if (_a >= 18.9) {
    if ((_rnd == 5) || (_rnd == 3)) {
      _a = 0.0;
    } else {
      _a = 6.29;
    }

    _k   = -12 + _rand->gen_int32() % 21;
    _l   = -15 + _rand->gen_int32() % 10;
    _m   = -15 + _rand->gen_int32() % 10;
    _rnd = -5 + _rand->gen_int32() % 30;
    _Rnd = _rand->gen_int32() % 25;
    _RND = _rand->gen_int32() % 1000;
    _k1  = _k;
  }

  const double range = (_k <= -7 || _k > 6) ? 0.005 : 0.01;
  _a += range;

  //
  // part 1
  _y = _H * 0.5 + 24.0 * cos(_a);
  _x = _W * 0.5 + 24.0 * sin(_a);

  if (_rnd > 5) {
    emit_vertex(_x, _y, updateCtx.linesBuffer);
  }

  if (_Rnd % 5 == 0)
    _rnd = 10;

  //
  // part 2
  _x = _W * 0.5 + 200.0 * sin(_a);
  _y = _H * 0.5 + 200.0 * cos(_a);

  if (_Rnd % 5 == 0)
    _k = _k1;

  int32_t o = _k;
  if ((_RND % 3 == 0) && (_Rnd % 5) != 0)
    o = (_k - 4) / 2;

  if ((_Rnd % 5) == 0 && (_k < -1 || _k > 4)) {
    if (_k > 4) {
      _Rnd = 1;
      _RND = 6;
    } else {
      _k = 2;
    }
  }

  //
  // part 4
  double  Rad  = 100.0;
  double  Rad_ = 100.0;
  int32_t rnd_{};
  int32_t rnd_cos_{};

  if (_a >= 18.9) {
    rnd_     = _rand->gen_int32() % 100;
    rnd_cos_ = -9 + _rand->gen_int32() % 15;
  }

  if (_rnd <= 5 && _a < 12.6) {
    Rad = Rad_ = 50.0;
  }

  if (rnd_ % 1 == 0)
    Rad = 100.0;

  if (rnd_ % 3 == 0)
    Rad = Rad_ * cos(_a * rnd_cos_);

  if (rnd_ % 4 == 0)
    Rad = Rad_ * cos(_a * _k);

  _y = _y + Rad * cos(_a * o);
  if (_Rnd % 5 == 0) {
    _x += Rad * sin(_a * (_k - 2) / 2) * cos(_a * (_k - 2) / 2);
  } else {
    _x += Rad * sin(_a * _k);
  }

  if (_Rnd % 1 == 0 && _a < 12.6) {
    emit_vertex(_x, _y, updateCtx.linesBuffer);
  }

  if (_a < 6.3 || (_a < 12.6 && _rnd > 5)) {
    emit_vertex(_x, _y, updateCtx.linesBuffer);
    emit_endpoint(_x, _y, updateCtx.endPointsBuffer);
  }

  if (_a <= 12.61 && _rnd <= 5) {
    const auto px = _x + 50.0 * sin(_a);
    const auto py = _y + 100.0 * cos(_a * _m);

    emit_vertex(px, py, updateCtx.linesBuffer);
    emit_endpoint(px, py, updateCtx.endPointsBuffer);
  }

  //
  // Part 5
  int32_t tmpData[51];
  tmpData[50] = _l;
  tmpData[30] = _m;
  for (int32_t A = 50; A >= 30; A -= 20) {
    _x = _x + A * sin(_a * tmpData[A]);
    _y = _y + A * cos(_a * tmpData[A]);

    if (_a >= 12.6) {
      if (_RND % 5 != 0) {
        if ((_Rnd % 5 == 0) && ((_k > -2 && _k < 2) || _k > 2)) {
          emit_endpoint(_x, _y, updateCtx.endPointsBuffer);
        } else {
          if ((_rnd <= 5) && (_Rnd % 5 != 0) && (_Rnd % 4 != 0)) {
            emit_vertex(_x, _y, updateCtx.linesBuffer);
          }
          emit_endpoint(_x, _y, updateCtx.endPointsBuffer);
        }
      } else {
        if ((_k < -2 || _k > 4) && (_a < 12.7)) {
          _k = -2 + _rand->gen_int32() % 5;
        }

        if (_Rnd % 4 != 0) {
          emit_endpoint(_x, _y, updateCtx.endPointsBuffer);
        } else {
          if ((_rnd <= 5) && (_Rnd % 5 != 0) && (_Rnd % 4 != 0)) {
            emit_vertex(_x, _y, updateCtx.linesBuffer);
          }
          emit_endpoint(_x, _y, updateCtx.endPointsBuffer);
        }
      }
    }
  }

  if ((_a >= 12.6) && (_rnd <= 5) && (_k > -5) && (_k < 5) && (_l > -9)) {
    _x = _x + 30 * sin(_a * _k);
    _y = _y + 30 * cos(_a * _k);
    emit_endpoint(_x, _y, updateCtx.endPointsBuffer);
  }

  if (_a >= 18.9) {
    _animState = animationState_t::finished;
  }
}

void mathArtAnimation3_t::generate_background() noexcept {
  if (_rand->gen_int32() % 2 == 0) {
    _backgroundColor.u32_clr = (4 + _rand->gen_int32() % 5) * 1000;
  } else {
    _backgroundColor.u32_clr = (13 + _rand->gen_int32() % 6) * 1000;
  }

  _backgroundColor.u32_clr += 990;
}

void mathArtAnimation3_t::options_ui(const uiContext_t& uiCtx) noexcept {
  auto     ctx = uiCtx.ctx;
  nk_panel animTab;
  nk_layout_row_static(ctx, 256.0f, 384.0f, 1);

  if (nk_group_begin(ctx, &animTab, "Animation options",
                     NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_BORDER |
                         NK_WINDOW_TITLE)) {

    uiDrawLineColorControls(uiCtx);

    nk_layout_row_dynamic(ctx, 32.0f, 2);
    {
      auto renderOpts = uiCtx.getRenderOptions();

      nk_label(ctx, "End caps color", NK_TEXT_LEFT);
      renderOpts->endCapsColor =
          nk_color_picker(ctx, renderOpts->endCapsColor, NK_RGB);

      nk_labelf(ctx, NK_TEXT_LEFT, "End caps size: [%3.3f]",
                renderOpts->endCapsSize);
      renderOpts->endCapsSize =
          nk_slide_float(ctx, 1.0f, renderOpts->endCapsSize, 10.0f, 0.1f);
    }

    nk_group_end(ctx);
  }
}

void mathArtAnimation3_t::emit_vertex(const double x, const double y,
                                      std::vector<xray::math::vec2>* buffer) {
  buffer->push_back({static_cast<float>(x), static_cast<float>(y)});
}

void mathArtAnimation3_t::emit_endpoint(const double x0, const double y0,
                                        std::vector<xray::math::vec2>* buffer) {
  emit_vertex(x0, y0, buffer);
}

/*!
 * \brief Contains rendering state and objects, to render the UI.
 */
struct uiRenderer_t {
  nk_buffer               commands;
  nk_draw_null_texture    nulltex;
  GLuint                  _vertbuff{};
  GLuint                  _vertarray{};
  GLuint                  _indexbuff{};
  GLsizei                 _vbuffsize{2048};
  GLsizei                 _ibuffsize{2048};
  rendering::gpuProgram_t _drawprog;
  GLint                   _uniform_tex{-1};
  GLint                   _uniform_proj_mtx{-1};
  GLuint                  _font_tex{};
  GLuint                  _sampler{};
  bool                    valid{false};

  uiRenderer_t() noexcept;
  ~uiRenderer_t() noexcept;

private:
  XR_NO_COPYCTOR_ASSIGNMENT(uiRenderer_t);
};

uiRenderer_t::~uiRenderer_t() noexcept {
  if (_vertbuff)
    gl::DeleteBuffers(1, &_vertbuff);

  if (_vertarray)
    gl::DeleteVertexArrays(1, &_vertarray);

  if (_indexbuff)
    gl::DeleteBuffers(1, &_indexbuff);

  if (_font_tex)
    gl::DeleteTextures(1, &_font_tex);

  if (_sampler)
    gl::DeleteSamplers(1, &_sampler);

  nk_buffer_free(&commands);
}

uiRenderer_t::uiRenderer_t() noexcept {
  nk_buffer_init_default(&commands);

  {
    gl::CreateBuffers(1, &_vertbuff);
    gl::NamedBufferData(_vertbuff, _vbuffsize, nullptr, gl::DYNAMIC_DRAW);
    assert(gl::IsBuffer(_vertbuff));
  }

  {
    gl::CreateBuffers(1, &_indexbuff);
    gl::NamedBufferData(_indexbuff, _ibuffsize, nullptr, gl::DYNAMIC_DRAW);
    assert(gl::IsBuffer(_indexbuff));
  }

  _vertarray = [vb = _vertbuff]() {
    GLuint vao{};
    gl::CreateVertexArrays(1, &vao);
    gl::VertexArrayVertexBuffer(vao, 0, vb, 0, sizeof(nk_draw_vertex));
    gl::VertexArrayAttribFormat(vao, 0, 2, gl::FLOAT, gl::FALSE_,
                                offsetof(nk_draw_vertex, position));
    gl::VertexArrayAttribFormat(vao, 1, 2, gl::FLOAT, gl::FALSE_,
                                offsetof(nk_draw_vertex, uv));
    gl::VertexArrayAttribFormat(vao, 2, 4, gl::UNSIGNED_BYTE, gl::TRUE_,
                                offsetof(nk_draw_vertex, col));
    gl::EnableVertexArrayAttrib(vao, 0);
    gl::EnableVertexArrayAttrib(vao, 1);
    gl::EnableVertexArrayAttrib(vao, 2);
    gl::VertexArrayAttribBinding(vao, 0, 0);
    gl::VertexArrayAttribBinding(vao, 1, 0);
    gl::VertexArrayAttribBinding(vao, 2, 0);

    assert(gl::IsVertexArray(vao));

    return vao;
  }
  ();

  _drawprog = []() {
    const char* const VERTEX_SHADER_CODE =
        "#version 450 core \n"
        "layout (location = 0) in vec2 vs_in_pos; \n"
        "layout (location = 1) in vec2 vs_in_uv; \n"
        "layout (location = 2) in vec4 vs_in_color; \n"
        "out VS_OUT_PS_IN { \n"
        "   layout (location = 0) vec2 uv; \n"
        "   layout (location = 1) vec4 col; \n"
        "} vs_out; \n"
        "uniform mat4 ModelViewProjection; \n"
        "\n"
        "void main() { \n"
        "    gl_Position = ModelViewProjection * vec4(vs_in_pos, 0.0, 1.0); \n"
        "    vs_out.uv = vs_in_uv; \n"
        "    vs_out.col = vs_in_color; \n"
        "} \n";

    const char* const FRAGMENT_SHADER_CODE =
        "#version 450 core \n"
        "uniform sampler2D FontTexture; \n"
        "in VS_OUT_PS_IN { \n"
        "   layout (location = 0) vec2 uv; \n"
        "   layout (location = 1) vec4 col; \n"
        "} fs_in; \n"
        "layout (location = 0) out vec4 FragColor; \n"
        "void main() { \n"
        "   FragColor = fs_in.col * texture(FontTexture, fs_in.uv.st); \n"
        "} \n";

    using namespace rendering;

    const drawingProgramComponent_t dpcomps[] = {
        {shaderRawStringSrc_t{VERTEX_SHADER_CODE}, gl::VERTEX_SHADER},
        {shaderRawStringSrc_t{FRAGMENT_SHADER_CODE}, gl::FRAGMENT_SHADER}};

    return gpuProgram_t{dpcomps, XR_ARRAY_SIZE(dpcomps)};
  }();

  if (!_drawprog)
    return;

  _uniform_proj_mtx =
      gl::GetUniformLocation(_drawprog.handle(), "ModelViewProjection");
  assert(_uniform_proj_mtx != -1);

  _uniform_tex = gl::GetUniformLocation(_drawprog.handle(), "FontTexture");
  assert(_uniform_tex != -1);

  valid = true;
}

/*!
 * \brief UI of the app.
 */
struct userInterface_t {
  SDL_Window*   _wnd;
  uiRenderer_t  _renderer;
  nk_context    _context;
  nk_font_atlas _atlas;

  userInterface_t(SDL_Window* wnd) noexcept;
  ~userInterface_t() noexcept;

  void render();
  void begin_input();
  void end_input();

  int32_t process_input_events(SDL_Event* ev);
};

void userInterface_t::begin_input() { nk_input_begin(&_context); }

void userInterface_t::end_input() { nk_input_end(&_context); }

int32_t userInterface_t::process_input_events(SDL_Event* evt) {
  nk_context* ctx = &_context;

  /* optional grabbing behavior */
  if (ctx->input.mouse.grab) {
    SDL_SetRelativeMouseMode(SDL_TRUE);
    ctx->input.mouse.grab = 0;
  } else if (ctx->input.mouse.ungrab) {
    int x = (int) ctx->input.mouse.prev.x, y = (int) ctx->input.mouse.prev.y;
    SDL_SetRelativeMouseMode(SDL_FALSE);
    SDL_WarpMouseInWindow(_wnd, x, y);
    ctx->input.mouse.ungrab = 0;
  }

  if (evt->type == SDL_KEYUP || evt->type == SDL_KEYDOWN) {
    /* key events */
    int          down  = evt->type == SDL_KEYDOWN;
    const Uint8* state = SDL_GetKeyboardState(0);
    SDL_Keycode  sym   = evt->key.keysym.sym;
    if (sym == SDLK_RSHIFT || sym == SDLK_LSHIFT)
      nk_input_key(ctx, NK_KEY_SHIFT, down);
    else if (sym == SDLK_DELETE)
      nk_input_key(ctx, NK_KEY_DEL, down);
    else if (sym == SDLK_RETURN)
      nk_input_key(ctx, NK_KEY_ENTER, down);
    else if (sym == SDLK_TAB)
      nk_input_key(ctx, NK_KEY_TAB, down);
    else if (sym == SDLK_BACKSPACE)
      nk_input_key(ctx, NK_KEY_BACKSPACE, down);
    else if (sym == SDLK_HOME) {
      nk_input_key(ctx, NK_KEY_TEXT_START, down);
      nk_input_key(ctx, NK_KEY_SCROLL_START, down);
    } else if (sym == SDLK_END) {
      nk_input_key(ctx, NK_KEY_TEXT_END, down);
      nk_input_key(ctx, NK_KEY_SCROLL_END, down);
    } else if (sym == SDLK_PAGEDOWN) {
      nk_input_key(ctx, NK_KEY_SCROLL_DOWN, down);
    } else if (sym == SDLK_PAGEUP) {
      nk_input_key(ctx, NK_KEY_SCROLL_UP, down);
    } else if (sym == SDLK_z)
      nk_input_key(ctx, NK_KEY_TEXT_UNDO, down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_r)
      nk_input_key(ctx, NK_KEY_TEXT_REDO, down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_c)
      nk_input_key(ctx, NK_KEY_COPY, down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_v)
      nk_input_key(ctx, NK_KEY_PASTE, down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_x)
      nk_input_key(ctx, NK_KEY_CUT, down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_b)
      nk_input_key(ctx, NK_KEY_TEXT_LINE_START,
                   down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_e)
      nk_input_key(ctx, NK_KEY_TEXT_LINE_END,
                   down && state[SDL_SCANCODE_LCTRL]);
    else if (sym == SDLK_LEFT) {
      if (state[SDL_SCANCODE_LCTRL])
        nk_input_key(ctx, NK_KEY_TEXT_WORD_LEFT, down);
      else
        nk_input_key(ctx, NK_KEY_LEFT, down);
    } else if (sym == SDLK_RIGHT) {
      if (state[SDL_SCANCODE_LCTRL])
        nk_input_key(ctx, NK_KEY_TEXT_WORD_RIGHT, down);
      else
        nk_input_key(ctx, NK_KEY_RIGHT, down);
    } else {
      return 0;
    }

    return 1;
  } else if (evt->type == SDL_MOUSEBUTTONDOWN ||
             evt->type == SDL_MOUSEBUTTONUP) {
    /* mouse button */
    int       down = evt->type == SDL_MOUSEBUTTONDOWN;
    const int x = evt->button.x, y = evt->button.y;
    if (evt->button.button == SDL_BUTTON_LEFT)
      nk_input_button(ctx, NK_BUTTON_LEFT, x, y, down);
    if (evt->button.button == SDL_BUTTON_MIDDLE)
      nk_input_button(ctx, NK_BUTTON_MIDDLE, x, y, down);
    if (evt->button.button == SDL_BUTTON_RIGHT)
      nk_input_button(ctx, NK_BUTTON_RIGHT, x, y, down);
    return 1;
  } else if (evt->type == SDL_MOUSEMOTION) {
    if (ctx->input.mouse.grabbed) {
      int x = (int) ctx->input.mouse.prev.x, y = (int) ctx->input.mouse.prev.y;
      nk_input_motion(ctx, x + evt->motion.xrel, y + evt->motion.yrel);
    } else
      nk_input_motion(ctx, evt->motion.x, evt->motion.y);
    return 1;
  } else if (evt->type == SDL_TEXTINPUT) {
    nk_glyph glyph;
    memcpy(glyph, evt->text.text, NK_UTF_SIZE);
    nk_input_glyph(ctx, glyph);
    return 1;
  } else if (evt->type == SDL_MOUSEWHEEL) {
    nk_input_scroll(ctx, (float) evt->wheel.y);
    return 1;
  }

  return 0;
}

userInterface_t::userInterface_t(SDL_Window* wnd) noexcept : _wnd{wnd} {
  nk_init_default(&_context, nullptr);

  nk_font_atlas_init_default(&_atlas);
  nk_font_atlas_begin(&_atlas);

  {
    int32_t    img_width{};
    int32_t    img_height{};
    const auto font_img = nk_font_atlas_bake(&_atlas, &img_width, &img_height,
                                             NK_FONT_ATLAS_RGBA32);

    gl::CreateTextures(gl::TEXTURE_2D, 1, &_renderer._font_tex);
    gl::TextureStorage2D(_renderer._font_tex, 1, gl::RGBA8, img_width,
                         img_height);
    gl::TextureSubImage2D(_renderer._font_tex, 0, 0, 0, img_width, img_height,
                          gl::RGBA, gl::UNSIGNED_BYTE, font_img);

    gl::CreateSamplers(1, &_renderer._sampler);
    gl::SamplerParameteri(_renderer._sampler, gl::TEXTURE_MIN_FILTER,
                          gl::LINEAR);
    gl::SamplerParameteri(_renderer._sampler, gl::TEXTURE_MAG_FILTER,
                          gl::LINEAR);
  }

  nk_font_atlas_end(&_atlas,
                    nk_handle_id(static_cast<int>(_renderer._font_tex)),
                    &_renderer.nulltex);

  if (_atlas.default_font)
    nk_style_set_font(&_context, &_atlas.default_font->handle);
}

userInterface_t::~userInterface_t() noexcept {
  nk_font_atlas_clear(&_atlas);
  nk_free(&_context);
}

void userInterface_t::render() {

  const auto wnd_size = [win = _wnd]() {
    int32_t w{};
    int32_t h{};
    SDL_GetWindowSize(win, &w, &h);
    return xray::math::i32vec2{w, h};
  }
  ();

  const auto surface_size = [win = _wnd]() {
    int32_t w{};
    int32_t h{};
    SDL_GL_GetDrawableSize(win, &w, &h);

    return xray::math::i32vec2{w, h};
  }
  ();

  rendering::openglStateSnapshotRestore_t opengl_state_snapshot{};

  gl::Viewport(0, 0, static_cast<GLsizei>(surface_size.x),
               static_cast<GLsizei>(surface_size.y));
  gl::Enable(gl::BLEND);
  gl::BlendEquation(gl::FUNC_ADD);
  gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
  gl::Disable(gl::CULL_FACE);
  gl::Disable(gl::DEPTH_TEST);
  gl::Enable(gl::SCISSOR_TEST);

  gl::BindSamplers(0, 1, &_renderer._sampler);
  gl::ProgramUniform1i(_renderer._drawprog.handle(), _renderer._uniform_tex, 0);

  const auto projection_matrix = xray::math::projection::ortographic(
      0.0f, static_cast<float>(surface_size.x), 0.0f,
      static_cast<float>(surface_size.y), 0.0f, 1.0f);

  gl::ProgramUniformMatrix4fv(_renderer._drawprog.handle(),
                              _renderer._uniform_proj_mtx, 1, gl::TRUE_,
                              projection_matrix.components);

  {
    static constexpr GLsizei MAX_VERTEX_BUFFER_SIZE = 1024 * 1024;
    static constexpr GLsizei MAX_INDEX_BUFFER_SIZE  = 1024 * 512;

    if (_renderer._vbuffsize < MAX_VERTEX_BUFFER_SIZE) {
      gl::NamedBufferData(_renderer._vertbuff, MAX_VERTEX_BUFFER_SIZE, nullptr,
                          gl::DYNAMIC_DRAW);
      _renderer._vbuffsize = MAX_VERTEX_BUFFER_SIZE;
    }

    if (_renderer._ibuffsize < MAX_INDEX_BUFFER_SIZE) {
      gl::NamedBufferData(_renderer._indexbuff, MAX_INDEX_BUFFER_SIZE, nullptr,
                          gl::DYNAMIC_DRAW);
      _renderer._ibuffsize = MAX_INDEX_BUFFER_SIZE;
    }

    rendering::scopedBufferMapping_t map_vertex{_renderer._vertbuff,
                                                gl::MAP_WRITE_BIT};
    assert(map_vertex);

    rendering::scopedBufferMapping_t map_ib{_renderer._indexbuff,
                                            gl::MAP_WRITE_BIT};
    assert(map_ib);

    auto vertices = map_vertex.data();
    auto indices  = map_ib.data();

    {
      nk_convert_config cfg;
      memset(&cfg, 0, sizeof(cfg));

      cfg.global_alpha         = 1.0f;
      cfg.shape_AA             = NK_ANTI_ALIASING_ON;
      cfg.line_AA              = NK_ANTI_ALIASING_ON;
      cfg.circle_segment_count = 22;
      cfg.curve_segment_count  = 22;
      cfg.arc_segment_count    = 22;
      cfg.null                 = _renderer.nulltex;

      nk_buffer vbuff;
      nk_buffer_init_fixed(&vbuff, vertices, _renderer._vbuffsize);
      nk_buffer ibuff;
      nk_buffer_init_fixed(&ibuff, indices, _renderer._ibuffsize);

      nk_convert(&_context, &_renderer.commands, &vbuff, &ibuff, &cfg);
    }
  }

  gl::BindVertexArray(_renderer._vertarray);
  gl::UseProgram(_renderer._drawprog.handle());
  gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, _renderer._indexbuff);

  {
    const auto scale = xray::math::vec2{
        static_cast<float>(surface_size.x) / static_cast<float>(wnd_size.x),
        static_cast<float>(surface_size.y) / static_cast<float>(wnd_size.y)};

    const nk_draw_command* cmd{};
    const nk_draw_index*   offset{};

    nk_draw_foreach(cmd, &_context, &_renderer.commands) {
      if (!cmd->elem_count)
        continue;

      const auto tex_id = static_cast<GLuint>(cmd->texture.id);
      gl::BindTextures(0, 1, &tex_id);
      gl::Scissor(static_cast<GLint>(cmd->clip_rect.x * scale.x),
                  static_cast<GLint>((wnd_size.y - (GLint)(cmd->clip_rect.y +
                                                           cmd->clip_rect.h)) *
                                     scale.y),
                  static_cast<GLint>(cmd->clip_rect.w * scale.x),
                  static_cast<GLint>(cmd->clip_rect.h * scale.y));

      gl::DrawElements(gl::TRIANGLES, static_cast<GLsizei>(cmd->elem_count),
                       gl::UNSIGNED_SHORT, offset);
      offset += cmd->elem_count;
    }
  }

  nk_clear(&_context);

  gl::BindVertexArray(0);
  gl::UseProgram(0);
}

/*!
 * \brief Main window of the app.
 */
class basic_window {
public:
  basic_window() noexcept;
  ~basic_window() noexcept;

  explicit operator bool() const noexcept { return valid(); }

  void message_loop();

  void postEvent(const windowEventCode_t wndEvent) noexcept;

private:
  static constexpr auto WINDOW_OPTS =
      SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL
      /*| SDL_WINDOW_INPUT_GRABBED*/;

  static constexpr auto RENDERER_OPTS =
      SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE;

  void processEvent(const SDL_UserEvent* uevt);

  bool valid() const noexcept { return _window && _rcontext.ctx; }

  void key_event(const SDL_KeyboardEvent* ke) noexcept;

  static Uint32 reset_event_callback(Uint32 interval, void*);

  static void opengl_debug_callback(GLenum source, GLenum type, GLuint id,
                                    GLenum severity, GLsizei length,
                                    const GLchar* message, const GLvoid* param);

  void process_ui(const int32_t drawableWidth,
                  const int32_t drawableHeight) noexcept;

  void cancel_timer() noexcept {
    if (_reset_timer != 0) {
      SDL_RemoveTimer(_reset_timer);
      _reset_timer = 0;
    }
  }

  void clear_buffers() noexcept {
    _vertices.clear();
    _endPoints.clear();
  }

  void set_animation(const animationType_t new_anim);

private:
  rendering::scoped_sdl_window     _window;
  rendering::scoped_sdl_gl_context _glcontext;

  struct renderContext_t {
    uint32_t      width{};
    uint32_t      height{};
    SDL_Window*   win{nullptr};
    SDL_GLContext ctx;
  } _rcontext;

  bool                              _quitflag{false};
  lowlevel::randomNumberGenerator_t _randgen{};
  SDL_TimerID                       _reset_timer{0};
  std::vector<xray::math::vec2>     _vertices;
  std::vector<xray::math::vec2>     _endPoints;

  struct animationInterface_t {
    lowlevel::fast_delegate<void(const animationUpdateContext_t&)> update;
    lowlevel::fast_delegate<void()>                                next_shape;
    lowlevel::fast_delegate<void()>                                reset;
    lowlevel::fast_delegate<void(const animationState_t)>          set_state;
    lowlevel::fast_delegate<animationState_t()>                    state;
    lowlevel::fast_delegate<colorType_t()>                         background;
    lowlevel::fast_delegate<void(const uiContext_t&)>              ui;
  } _animationControl;

  mathArtAnimation_t  _anim_type1{&_randgen};
  mathArtAnimation2_t _anim_type2{&_randgen};
  mathArtAnimation3_t _animTypeC{&_randgen};
  animationType_t     _active_anim{animationType_t::kTypeA};

  std::unique_ptr<app::animationRenderer_t> _animationRenderer{};
  std::unique_ptr<app::userInterface_t>     _ui{};

private:
  XR_NO_COPYCTOR_ASSIGNMENT(basic_window);
};

basic_window::basic_window() noexcept {
  _vertices.reserve(2048);
  _endPoints.reserve(2048);

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,
                      SDL_GL_CONTEXT_DEBUG_FLAG |
                          SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);

  _window = rendering::scoped_sdl_window{SDL_CreateWindow(
      "Math art animation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 0,
      0, basic_window::WINDOW_OPTS)};

  if (!_window)
    return;

  _glcontext = rendering::scoped_sdl_gl_context{
      SDL_GL_CreateContext(lowlevel::raw_ptr(_window))};

  if (!_glcontext) {
    OUT_DBG_MSG("Failed to create GL context !!");
    return;
  }

  _rcontext.win = lowlevel::raw_ptr(_window);
  _rcontext.ctx = lowlevel::raw_ptr(_glcontext);

  SDL_GL_MakeCurrent(_rcontext.win, _rcontext.ctx);

  if (!gl::sys::LoadFunctions()) {
    OUT_DBG_MSG("Failed to load OpenGL!!");
    return;
  }

  OUT_DBG_MSG("Loaded OpenGL %d.%d", gl::sys::GetMajorVersion(),
              gl::sys::GetMinorVersion());

  gl::DebugMessageCallback(&basic_window::opengl_debug_callback, this);
  gl::DebugMessageControl(gl::DONT_CARE, gl::DONT_CARE,
                          gl::DEBUG_SEVERITY_NOTIFICATION, 0, nullptr,
                          gl::FALSE_);

  set_animation(animationType_t::kTypeA);
  _animationRenderer = std::make_unique<animationRenderer_t>();
  _ui                = std::make_unique<userInterface_t>(_rcontext.win);
}

basic_window::~basic_window() noexcept {
  cancel_timer();
  if (_glcontext) {
    SDL_GL_MakeCurrent(_rcontext.win, nullptr);
  }
}

void basic_window::processEvent(const SDL_UserEvent* uevt) {
  const auto eventCode = static_cast<windowEventCode_t>(uevt->code);

  if (eventCode == windowEventCode_t::kAnimationSwitchByUser) {
    cancel_timer();
    return;
  }

  if (eventCode == windowEventCode_t::kAnimationSwitchAutomatic) {
    cancel_timer();
    _animationControl.next_shape();
    return;
  }
}

void basic_window::message_loop() {
  bool wndHidden{false};

  for (; _quitflag == false;) {
    SDL_Event event;

    _ui->begin_input();

    while (SDL_PollEvent(&event) && !_quitflag) {
      if (event.type == SDL_QUIT) {
        _quitflag = true;
        break;
      }

      if (event.type == SDL_WINDOWEVENT) {
        const auto wndEventType = event.window.event;

        switch (wndEventType) {
        case SDL_WINDOWEVENT_HIDDEN:
        case SDL_WINDOWEVENT_MINIMIZED:
          wndHidden = true;
          break;

        case SDL_WINDOWEVENT_MAXIMIZED:
        case SDL_WINDOWEVENT_RESTORED:
          wndHidden = false;

        default:
          break;
        }
      }

      _ui->process_input_events(&event);

      if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
        key_event(&event.key);
      }

      if (event.type == SDL_USEREVENT) {
        processEvent(&event.user);
        continue;
      }
    }

    _ui->end_input();

    if (wndHidden) {
      SDL_Delay(100);
      continue;
    }

    xray::math::i32vec2 drawableSurfaceSize;
    SDL_GL_GetDrawableSize(_rcontext.win, &drawableSurfaceSize.x,
                           &drawableSurfaceSize.y);

    if (_animationControl.state() == animationState_t::not_started) {
      clear_buffers();
      _animationControl.set_state(animationState_t::running);
    }

    if (_animationControl.state() == animationState_t::running) {
      const animationUpdateContext_t updateCtx = {
          static_cast<float>(drawableSurfaceSize.x),
          static_cast<float>(drawableSurfaceSize.y), &_vertices, &_endPoints};

      _animationControl.update(updateCtx);
    }

    if (_animationControl.state() == animationState_t::finished) {
      assert(_reset_timer == 0);
      _reset_timer =
          SDL_AddTimer(10 * 1000, &basic_window::reset_event_callback, nullptr);
      _animationControl.set_state(animationState_t::paused);
    }

    {
      const float clr_color[] = {_animationControl.background().red(),
                                 _animationControl.background().green(),
                                 _animationControl.background().blue(), 1.0f};

      gl::ClearBufferfv(gl::COLOR, 0, clr_color);
      const float depth_val{1.0f};
      gl::ClearBufferfv(gl::DEPTH, 0, &depth_val);
    }

    if (_vertices.size() > 1) {
      animationRenderer_t::renderContext_t rcon;
      rcon.verticesLines          = &_vertices[0];
      rcon.linesVerticesCount     = _vertices.size();
      rcon.verticesEndPoints      = &_endPoints[0];
      rcon.endPointsVerticesCount = _endPoints.size();
      rcon.animType               = _active_anim;
      rcon.viewProjectionMatrix   = xray::math::projection::ortographic(
          0.0f, (float) drawableSurfaceSize.x, 0.0f,
          (float) drawableSurfaceSize.y, 0.0f, 1.0f);

      _animationRenderer->draw(&rcon);
    }

    process_ui(drawableSurfaceSize.x, drawableSurfaceSize.y);
    SDL_GL_SwapWindow(_rcontext.win);
  }
}

void basic_window::key_event(const SDL_KeyboardEvent* ke) noexcept {
  if (ke->state == SDL_PRESSED) {
    const auto keysym = ke->keysym.sym;

    switch (keysym) {
    case SDLK_ESCAPE:
      _quitflag = true;
      break;

    default:
      break;
    }
  }
}

Uint32 basic_window::reset_event_callback(Uint32 interval, void*) {
  SDL_Event     evt;
  SDL_UserEvent user_ev;

  user_ev.type = SDL_USEREVENT;
  user_ev.code =
      static_cast<Sint32>(windowEventCode_t::kAnimationSwitchAutomatic);
  user_ev.data1 = nullptr;
  user_ev.data2 = nullptr;

  evt.type = SDL_USEREVENT;
  evt.user = user_ev;

  SDL_PushEvent(&evt);
  return interval;
}

void basic_window::opengl_debug_callback(GLenum /*source*/, GLenum /*type*/,
                                         GLuint /*id*/, GLenum /*severity*/,
                                         GLsizei /*length*/,
                                         const GLchar* message,
                                         const GLvoid* /*param*/) {
  OUT_DBG_MSG(":::: OpenGL Message ::::\n[%s]", message);
}

void basic_window::process_ui(const int32_t drawableWidth,
                              const int32_t drawableHeight) noexcept {
  using namespace lowlevel;
  nk_context* ctx = &_ui->_context;

  XR_SCOPE_EXIT() {
    nk_end(ctx);
    _ui->render();
  };

  struct nk_panel layout;
  if (nk_begin_titled(ctx, &layout, "Control Panel", "Control Panel",
                      nk_rect(0, 0, 420, 820),
                      NK_WINDOW_BORDER | NK_WINDOW_SCALABLE |
                          NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {

    nk_layout_row_static(ctx, 32.0f, 128.0f, 1);

    static bool showAboutDlg{false};

    if (showAboutDlg) {
      const auto wndCenter =
          xray::math::i32vec2{drawableWidth / 2, drawableHeight / 2};

      const auto kPopupWidth  = 640;
      const auto kPopupHeight = 300;

      assert(wndCenter.x >= kPopupWidth / 2);
      assert(wndCenter.y >= kPopupHeight / 2);

      const auto popupRect =
          nk_rect(wndCenter.x - kPopupWidth / 2, wndCenter.y - kPopupHeight / 2,
                  kPopupWidth, kPopupHeight);

      struct nk_panel aboutDlg;
      if (nk_popup_begin(ctx, &aboutDlg, NK_POPUP_STATIC, "OpenGL animations",
                         NK_WINDOW_CLOSABLE, popupRect)) {

        nk_layout_row_dynamic(ctx, 24.0f, 1);
        nk_label(ctx, "OpenGL (4.5) & beautifull math", NK_TEXT_CENTERED);
        nk_label(ctx, "(c) Hodos Adrian, 2016", NK_TEXT_CENTERED);
        nk_label(ctx,
                 "Code for this app adapted from this CodeProject article :",
                 NK_TEXT_CENTERED);
        nk_label(ctx, "http://www.codeproject.com/Articles/300388/"
                      "Using-C-to-blend-Mathematics-and-Art-When-Math-goe",
                 NK_TEXT_CENTERED);

        nk_label(ctx, "Aditional awesome software used for this app :",
                 NK_TEXT_CENTERED);

        nk_label(
            ctx,
            "FastDelegate library : http://www.codeproject.com/Articles/7150/"
            "Member-Function-Pointers-and-the-Fastest-Possible",
            NK_TEXT_CENTERED);

        nk_label(ctx, "Nuklear UI library : https://github.com/vurtun/nuklear",
                 NK_TEXT_CENTERED);

        nk_label(ctx, "SDL2 Library : https://www.libsdl.org/",
                 NK_TEXT_CENTERED);

        nk_label(ctx, "OpenGL Loader Generator : "
                      "https://bitbucket.org/alfonse/glloadgen/wiki/Home",
                 NK_TEXT_CENTERED);

        nk_popup_end(ctx);

        nk_button_label(ctx, "About");
      } else {
        showAboutDlg = false;
      }
    } else {
      showAboutDlg = nk_button_label(ctx, "About") != 0;
    }

    nk_layout_row_static(ctx, 200, 380, 1);

    //
    // RNG settings group
    {
      nk_panel tab;
      if (nk_group_begin(ctx, &tab, "Random number generator settings",
                         NK_WINDOW_BORDER | NK_WINDOW_NO_SCROLLBAR |
                             NK_WINDOW_TITLE)) {

        nk_layout_row_dynamic(ctx, 30, 2);
        const auto dist = _randgen.distribution();

        for (int32_t i = static_cast<int32_t>(
                 randomNumberGenerator_t::distributionType_t::first);
             i < static_cast<int32_t>(
                     randomNumberGenerator_t::distributionType_t::last);
             ++i) {

          if (nk_option_label(ctx,
                              randomNumberGenerator_t::DISTRIBUTION_NAMES[i],
                              static_cast<int32_t>(dist) == i) &&
              (static_cast<int32_t>(dist) != i)) {

            _randgen.set_distribution(
                static_cast<randomNumberGenerator_t::distributionType_t>(i));

            if (_animationControl.state() == animationState_t::running) {
              _animationControl.reset();
            }
          }
        }

        nk_group_end(ctx);
      }
    }

    //
    //  Animation selection group
    {
      nk_layout_row_static(ctx, 164.0f, 384.0f, 1);
      nk_panel grp_tab;
      if (nk_group_begin(ctx, &grp_tab, "Animation type",
                         NK_WINDOW_BORDER | NK_WINDOW_NO_SCROLLBAR |
                             NK_WINDOW_TITLE)) {

        nk_layout_row_dynamic(ctx, 32.0f, 1);

        if (nk_option_label(ctx, "Type A",
                            _active_anim == animationType_t::kTypeA) &&
            (_active_anim != animationType_t::kTypeA)) {
          set_animation(animationType_t::kTypeA);
        }

        if (nk_option_label(ctx, "Type B",
                            _active_anim == animationType_t::kTypeB) &&
            (_active_anim != animationType_t::kTypeB)) {
          set_animation(animationType_t::kTypeB);
        }

        if (nk_option_label(ctx, "Type C",
                            _active_anim == animationType_t::kTypeC) &&
            (_active_anim != animationType_t::kTypeC)) {
          set_animation(animationType_t::kTypeC);
        }

        nk_group_end(ctx);
      }
    }

    {
      const uiContext_t uiCtx{
          ctx, lowlevel::make_delegate(*this, &basic_window::postEvent),
          lowlevel::make_delegate(*_animationRenderer,
                                  &animationRenderer_t::getRenderOptions)};

      _animationControl.ui(uiCtx);
    }
  }
}

void basic_window::set_animation(const animationType_t new_anim) {
  _active_anim = new_anim;

  if (new_anim == animationType_t::kTypeA) {
    _animationControl.reset =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::reset);
    _animationControl.set_state =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::set_state);
    _animationControl.next_shape =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::next_shape);
    _animationControl.update =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::update);
    _animationControl.state =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::state);
    _animationControl.background =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::background);
    _animationControl.ui =
        lowlevel::make_delegate(_anim_type1, &mathArtAnimation_t::options_ui);
  } else if (new_anim == animationType_t::kTypeB) {
    _animationControl.reset =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::reset);
    _animationControl.set_state =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::set_state);
    _animationControl.next_shape =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::next_shape);
    _animationControl.update =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::update);
    _animationControl.state =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::state);
    _animationControl.background =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::background);
    _animationControl.ui =
        lowlevel::make_delegate(_anim_type2, &mathArtAnimation2_t::options_ui);
  } else if (new_anim == animationType_t::kTypeC) {
    _animationControl.reset =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::reset);
    _animationControl.set_state =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::set_state);
    _animationControl.next_shape =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::next_shape);
    _animationControl.update =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::update);
    _animationControl.state =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::state);
    _animationControl.background =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::background);
    _animationControl.ui =
        lowlevel::make_delegate(_animTypeC, &mathArtAnimation3_t::options_ui);
  }

  clear_buffers();
  _animationControl.reset();
}

void basic_window::postEvent(const windowEventCode_t wndEvent) noexcept {
  SDL_UserEvent user_ev;
  user_ev.type  = SDL_USEREVENT;
  user_ev.code  = static_cast<Sint32>(wndEvent);
  user_ev.data1 = nullptr;
  user_ev.data2 = nullptr;

  SDL_Event evt;
  evt.type = SDL_USEREVENT;
  evt.user = user_ev;

  SDL_PushEvent(&evt);
}

} // namespace app

#if defined(_WIN32)
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
#else
int main(int, char**) {
#endif

  const rendering::scoped_sdllib_initializer sdl_initialized{};

  if (!sdl_initialized) {
    return EXIT_FAILURE;
  }

  app::basic_window wnd{};
  if (!wnd) {
    return EXIT_FAILURE;
  }

  wnd.message_loop();
  return 0;
}
