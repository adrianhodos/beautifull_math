TEMPLATE = app
CONFIG += windows
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++14 -Wall -Wextra

LIBS += -lSDL2

win32 {
LIBS += -luser32 -lgdi32 -lwinmm -limm32 -lole32 -loleaut32 -lshell32 -lversion -luuid -lopengl32
}

unix {
LIBS += -ldl -lGL
}

SOURCES += \
    src/main.cc \
    src/glad.c \
    src/gl_api.cpp \
    src/nuklear.cc

INCLUDEPATH += $$PWD/include

HEADERS += \
    include/gl_api.hpp \
    include/nuklear.h \
    include/nk_config.hpp \
    include/fast_delegate.hpp \
    include/xray_math.hpp
