#version 450 core

uniform sampler2D FontTexture;

in VS_OUT_PS_IN {
    layout (location = 0) vec2 uv;
    layout (location = 1) vec4 col;
} fs_in;

layout (location = 0) out vec4 FragColor;

void main() {
    FragColor = fs_in.col * texture(FontTexture, uv.st);
}