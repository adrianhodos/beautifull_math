#version 450 core

layout (row_major) uniform;
layout (location = 0) in vec2 vs_in_pos;
layout (location = 1) in vec2 vs_in_uv;
layout (location = 2) in vec4 vs_in_color;

out VS_OUT_PS_IN {
    layout (location = 0) vec2 uv;
    layout (location = 1) vec4 col;
} vs_out;

uniform mat4 projection;

void main() {
    gl_Position = projection * vec4(vs_in_pos, 0.0, 1.0);
    vs_out.uv = vs_in_uv;
    vs_out.col = vs_in_color;
}